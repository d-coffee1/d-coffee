/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.productcomponent;

import com.mycompany.dcoffeeproject.model.Product;


/**
 *
 * @author ส้มส้ม
 */
public interface BuyProductable {
    public void buy(Product product,String name, String size, String type, String sweet,Double price,int qty);
}
