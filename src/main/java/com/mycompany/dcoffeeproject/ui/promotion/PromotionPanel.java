/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.promotion;

import com.mycompany.dcoffeeproject.model.Promotion;
import com.mycompany.dcoffeeproject.service.PromotionService;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author ส้มส้ม
 */
public class PromotionPanel extends javax.swing.JPanel {

    private final AbstractTableModel model;
    private List<Promotion> promotions;
    private PromotionService promotionService = new PromotionService();

    public List<Promotion> getPromotions() {
        return promotions;
    }

    public void setPromotions(List<Promotion> promotions) {
        this.promotions = promotions;
    }

    public AbstractTableModel getModel() {
        return model;
    }

//    /**
//     * Creates new form ProductPanel
//     */
    public PromotionPanel() {
        initComponents();
        JTableHeader header = tbPromotion.getTableHeader();
        header.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        header.setBackground(Color.decode("#7c3893"));
        header.setForeground(Color.decode("#f3edf5"));
        promotions = promotionService.getPromotions();
        model = new AbstractTableModel() {
            String[] columnNames = {"รหัสโปรโมชั่น", "ชื่อ", "รายละเอียดโปรโมชั่น", "วันเริ่มต้น", "วันสิ้นสุด"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return promotions.size();
            }

            @Override
            public int getColumnCount() {
                return 5;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Promotion promotion = promotions.get(rowIndex);
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                String startDate = formatter.format(promotion.getStartDate());
                String endDate = formatter.format(promotion.getEndDate());
                switch (columnIndex) {
                    case 0:
                        return promotion.getId();
                    case 1:
                        return promotion.getName();
                    case 2:
                        return promotion.getDiscription();
                    case 3:
                        return startDate;
                    case 4:
                        return endDate;
                    default:
                        return "Unknown";
                }
            }
        };
        tbPromotion.setModel(model);
        setSize(WIDTH, HEIGHT);

    }
//
//    /**
//     * This method is called from within the constructor to initialize the form.
//     * WARNING: Do NOT modify this code. The content of this method is always
//     * regenerated by the Form Editor.
//     */
//    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tbPromotion = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        btnAddPromotion = new javax.swing.JButton();
        btnEditPromotion = new javax.swing.JButton();
        btnDeletePromotion = new javax.swing.JButton();
        txtSearch = new javax.swing.JTextField();
        btnSearch = new javax.swing.JButton();

        setBackground(new java.awt.Color(245, 237, 245));

        tbPromotion.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbPromotion.setForeground(new java.awt.Color(52, 52, 52));
        tbPromotion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbPromotion);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(52, 52, 52));
        jLabel1.setText("รายการโปรโมชั่น");

        btnAddPromotion.setBackground(new java.awt.Color(124, 56, 147));
        btnAddPromotion.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnAddPromotion.setForeground(new java.awt.Color(243, 237, 245));
        btnAddPromotion.setText("เพิ่มโปรโมชั่น");
        btnAddPromotion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddPromotionActionPerformed(evt);
            }
        });

        btnEditPromotion.setBackground(new java.awt.Color(124, 56, 147));
        btnEditPromotion.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnEditPromotion.setForeground(new java.awt.Color(243, 237, 245));
        btnEditPromotion.setText("แก้ไขโปรโมชั่น");
        btnEditPromotion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditPromotionActionPerformed(evt);
            }
        });

        btnDeletePromotion.setBackground(new java.awt.Color(124, 56, 147));
        btnDeletePromotion.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnDeletePromotion.setForeground(new java.awt.Color(243, 237, 245));
        btnDeletePromotion.setText("ลบโปรโมชั่น");
        btnDeletePromotion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnDeletePromotionActionPerformed(evt);
            }
        });

        txtSearch.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtSearch.setForeground(new java.awt.Color(52, 52, 52));
        txtSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSearchActionPerformed(evt);
            }
        });

        btnSearch.setBackground(new java.awt.Color(124, 56, 147));
        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(243, 237, 245));
        btnSearch.setText("ค้นหา");
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 287, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 1058, Short.MAX_VALUE))
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnAddPromotion, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnEditPromotion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnDeletePromotion)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(16, 16, 16)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnAddPromotion, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEditPromotion, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnDeletePromotion, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 564, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void txtSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSearchActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSearchActionPerformed

    private void btnAddPromotionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddPromotionActionPerformed
        openAddPromotionDialog(new Promotion());
    }//GEN-LAST:event_btnAddPromotionActionPerformed

    private void openAddPromotionDialog(Promotion promotion) {
        AddPromotionDialog addPromotion = new AddPromotionDialog(this, promotion);
        addPromotion.setLocationRelativeTo(this);
        addPromotion.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshTable();
            }

        });
        addPromotion.setVisible(true);
    }

    private void btnEditPromotionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditPromotionActionPerformed
        int selectedIndex = tbPromotion.getSelectedRow();

        if (selectedIndex >= 0) {
            Promotion selectedPromotion = promotions.get(selectedIndex);
            openAddPromotionDialog(selectedPromotion);

        }
    }//GEN-LAST:event_btnEditPromotionActionPerformed

    private void btnDeletePromotionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnDeletePromotionActionPerformed
        int selectedIndex = tbPromotion.getSelectedRow();
        if (selectedIndex >= 0) {
            Promotion deletedPromotion = promotions.get(selectedIndex);
            Object[] options = {"ใช่", "ไม่"};
            int input = JOptionPane.showOptionDialog(
                    this,
                    "ยืนยันที่จะทำรายการหรือไม่?",
                    "โปรดเลือก ...",
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.ERROR_MESSAGE,
                    null,
                    options,
                    options[1]
            );
            if (input == 0) {
                promotionService.delete(deletedPromotion);

                refreshTable();
            }
        }
    }//GEN-LAST:event_btnDeletePromotionActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        promotions = promotionService.searchProductsByName(txtSearch.getText());
        model.fireTableDataChanged();
    }//GEN-LAST:event_btnSearchActionPerformed

    public void refreshTable() {
        promotions = promotionService.getPromotions();
        model.fireTableDataChanged();
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAddPromotion;
    private javax.swing.JButton btnDeletePromotion;
    private javax.swing.JButton btnEditPromotion;
    private javax.swing.JButton btnSearch;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JScrollPane jScrollPane1;
    public static javax.swing.JTable tbPromotion;
    private javax.swing.JTextField txtSearch;
    // End of variables declaration//GEN-END:variables

}
