/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.material;

import com.mycompany.dcoffeeproject.DateLabelFormatter;
import com.mycompany.dcoffeeproject.model.CheckMaterial;
import com.mycompany.dcoffeeproject.model.CheckMaterialItem;
import com.mycompany.dcoffeeproject.service.CheckMaterialService;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author ส้มส้ม
 */
public class CheckMatPanel extends javax.swing.JPanel {

    private final AbstractTableModel checkMaterialItemModel;
    private final AbstractTableModel checkMaterialModel;
    private CheckMaterialService checkMaterialService = new CheckMaterialService();
    private List<CheckMaterial> checkMaterials;
    private List<CheckMaterialItem> checkMaterialItems;
    private UtilDateModel startDateModel;
    private UtilDateModel endDateModel;

    /**
     * Creates new form CheckMatPanel
     */
    public CheckMatPanel() {
        initComponents();
        JTableHeader headerCheckMat = tbCheckMat.getTableHeader();
        headerCheckMat.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        headerCheckMat.setBackground(Color.decode("#7c3893"));
        headerCheckMat.setForeground(Color.decode("#f3edf5"));

        JTableHeader headerCheckMatItem = tbCheckMatItem.getTableHeader();
        headerCheckMatItem.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        headerCheckMatItem.setBackground(Color.decode("#7c3893"));
        headerCheckMatItem.setForeground(Color.decode("#f3edf5"));
        setSize(WIDTH, HEIGHT);
        checkMaterials = checkMaterialService.getCheckMaterials();
        initDatePicker();
        checkMaterialModel = new AbstractTableModel() {
            String[] columnNames = {"วันที่", "ชื่อ -  นามสกุล"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return checkMaterials.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckMaterial checkMaterial = checkMaterials.get(rowIndex);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                String checkMatDate = dateFormat.format(checkMaterial.getDate());
     
                switch (columnIndex) {
                    case 0:
                        return checkMatDate;
                    case 1:
                        return checkMaterial.getEmployee().getName() + " " + checkMaterial.getEmployee().getSurname();
                    default:
                        return "Unknown";
                }
            }
        };
        tbCheckMat.setModel(checkMaterialModel);

        checkMaterialItemModel = new AbstractTableModel() {
            String[] columnNames = {"ชื่อวัตถุดิบ", "ยอดคงเหลือครั้งที่แล้ว", "ยอดคงเหลือปัจจุบัน"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return checkMaterialItems == null ? 0 : checkMaterialItems.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckMaterialItem checkMaterialItem = checkMaterialItems.get(rowIndex);
                DecimalFormat decimalFormat = new DecimalFormat("#,##0");
                String formattedValueLastBalance = decimalFormat.format(checkMaterialItem.getLastBalance());
                String formattedValueCurrentBalance = decimalFormat.format(checkMaterialItem.getCurrentBalance());
                switch (columnIndex) {
                    case 0:
                        return checkMaterialItem == null ? "" : checkMaterialItem.getMaterial().getName();
                    case 1:
                        return checkMaterialItem == null ? "" : formattedValueLastBalance;
                    case 2:
                        return checkMaterialItem == null ? "" : formattedValueCurrentBalance;
                    default:
                        return "Unknown";
                }
            }
        };
        tbCheckMatItem.setModel(checkMaterialItemModel);
    }

    private void initDatePicker() {
        Font tahomaFont = new Font("Tahoma", Font.PLAIN, 12);
        startDateModel = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("text.today", "วัน");
        p1.put("text.month", "เดือน");
        p1.put("text.year", "ปี");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(startDateModel, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
        pnlStartDatePicker.add(datePicker1);

        endDateModel = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("text.today", "วัน");
        p2.put("text.month", "เดือน");
        p2.put("text.year", "ปี");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(endDateModel, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        pnlEndDatePicker.add(datePicker2);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        btnEditBalanceMat = new javax.swing.JButton();
        btnPrintMat = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbCheckMatItem = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbCheckMat = new javax.swing.JTable();
        btnViewItem = new javax.swing.JButton();
        pnlStartDatePicker = new javax.swing.JPanel();
        pnlEndDatePicker = new javax.swing.JPanel();
        btnSearch = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(245, 237, 245));
        jPanel1.setPreferredSize(new java.awt.Dimension(1357, 683));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("รายการตรวจสอบวัตถุดิบ");
        jLabel1.setPreferredSize(new java.awt.Dimension(267, 29));

        btnEditBalanceMat.setBackground(new java.awt.Color(124, 56, 147));
        btnEditBalanceMat.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnEditBalanceMat.setForeground(new java.awt.Color(243, 237, 245));
        btnEditBalanceMat.setText("ปรับปรุงยอดคงเหลือ");
        btnEditBalanceMat.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnEditBalanceMat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEditBalanceMatActionPerformed(evt);
            }
        });

        btnPrintMat.setBackground(new java.awt.Color(124, 56, 147));
        btnPrintMat.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnPrintMat.setForeground(new java.awt.Color(243, 237, 245));
        btnPrintMat.setText("พิมพ์รายการวัตถุดิบ");
        btnPrintMat.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnPrintMat.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPrintMatActionPerformed(evt);
            }
        });

        tbCheckMatItem.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbCheckMatItem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbCheckMatItem);

        tbCheckMat.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbCheckMat.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tbCheckMat);

        btnViewItem.setBackground(new java.awt.Color(124, 56, 147));
        btnViewItem.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnViewItem.setForeground(new java.awt.Color(243, 237, 245));
        btnViewItem.setText("ดูรายละเอียด");
        btnViewItem.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnViewItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewItemActionPerformed(evt);
            }
        });

        pnlStartDatePicker.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        pnlStartDatePicker.setOpaque(false);

        pnlEndDatePicker.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        pnlEndDatePicker.setOpaque(false);

        btnSearch.setBackground(new java.awt.Color(124, 56, 147));
        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(243, 237, 245));
        btnSearch.setText("ค้นหา");
        btnSearch.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(pnlStartDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(pnlEndDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 99, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnViewItem))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 689, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 921, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnEditBalanceMat)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnPrintMat)))
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 431, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(pnlEndDatePicker, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlStartDatePicker, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSearch)
                        .addComponent(btnViewItem)
                        .addComponent(btnEditBalanceMat)
                        .addComponent(btnPrintMat)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 795, Short.MAX_VALUE)
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1628, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 857, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(41, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewItemActionPerformed
        int index = tbCheckMat.getSelectedRow();
        if (index >= 0) {
            checkMaterialItems = checkMaterials.get(index).getCheckMaterialItems();
        }
        refreshTable();
    }//GEN-LAST:event_btnViewItemActionPerformed

    private void btnPrintMatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPrintMatActionPerformed
        CheckMaterialSheetDialog cms = new CheckMaterialSheetDialog();
        cms.setLocationRelativeTo(this);
        cms.setVisible(true);
    }//GEN-LAST:event_btnPrintMatActionPerformed

    private void btnEditBalanceMatActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEditBalanceMatActionPerformed
        EditMatBalanceDialog emb = new EditMatBalanceDialog(this);
        emb.setLocationRelativeTo(this);
        emb.addWindowListener(new WindowAdapter(){
            @Override
            public void windowClosed(WindowEvent e) {
                checkMaterials = checkMaterialService.getCheckMaterials();
                refreshTable();
            }
            
        });
        emb.setVisible(true);
    }//GEN-LAST:event_btnEditBalanceMatActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        if (startDateModel.getValue() != null && endDateModel.getValue() == null) {
            JOptionPane.showMessageDialog(this, "กรุณาเลือกวันสิ้นสุด");
        } else if (startDateModel.getValue() == null && endDateModel.getValue() != null) {
            JOptionPane.showMessageDialog(this, "กรุณาเลือกวันเริ่มต้น");
        } else if (startDateModel.getValue() == null && endDateModel.getValue() == null) {
            checkMaterials = checkMaterialService.getCheckMaterials();
        } else if (startDateModel.getValue() != null && endDateModel.getValue() != null) {
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            String start = formater.format(startDateModel.getValue());
            String end = formater.format(endDateModel.getValue());
            checkMaterials = checkMaterialService.getCheckMaterials(start, end);
        }
        refreshTable();
    }//GEN-LAST:event_btnSearchActionPerformed

    public void refreshTable() {
        checkMaterialModel.fireTableDataChanged();
        checkMaterialItemModel.fireTableDataChanged();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnEditBalanceMat;
    private javax.swing.JButton btnPrintMat;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnViewItem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPanel pnlEndDatePicker;
    private javax.swing.JPanel pnlStartDatePicker;
    private javax.swing.JTable tbCheckMat;
    private javax.swing.JTable tbCheckMatItem;
    // End of variables declaration//GEN-END:variables
}
