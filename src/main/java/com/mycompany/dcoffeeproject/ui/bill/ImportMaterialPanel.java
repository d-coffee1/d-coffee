/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.bill;

import com.mycompany.dcoffeeproject.DateLabelFormatter;
import com.mycompany.dcoffeeproject.ui.billcomponent.BuyMaterialable;
import com.mycompany.dcoffeeproject.ui.billcomponent.MaterialListPanel;
import com.mycompany.dcoffeeproject.model.Bill;
import com.mycompany.dcoffeeproject.model.BillItem;
import com.mycompany.dcoffeeproject.model.Material;
import com.mycompany.dcoffeeproject.model.Supplier;
import com.mycompany.dcoffeeproject.service.BillService;
import com.mycompany.dcoffeeproject.service.EmployeeService;
import com.mycompany.dcoffeeproject.service.MaterialService;
import com.mycompany.dcoffeeproject.service.SupplierService;
import com.mycompany.dcoffeeproject.service.ValidateException;
import com.mycompany.dcoffeeproject.ui.member.AddMemberDialog;
import java.awt.Color;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author ส้มส้ม
 */
public class ImportMaterialPanel extends javax.swing.JPanel implements BuyMaterialable {

    private ArrayList<Material> materials;
    private EmployeeService employeeService = new EmployeeService();
    private BillService billService = new BillService();
    private Bill bill;
    private final MaterialListPanel materialListPanel = new MaterialListPanel();
    private MaterialService materialService = new MaterialService();
    private List<Supplier> suppliers = new ArrayList<>();
    private SupplierService supplierService = new SupplierService();
    private UtilDateModel model;

    public Bill getBill() {
        return bill;
    }

    public void setBill(Bill bill) {
        this.bill = bill;
    }

    public ImportMaterialPanel() {
        initComponents();
        initMaterialTable();
        btnCalDiscount.setBackground(Color.decode("#b465c8"));
        suppliers = supplierService.getSuppliers();
        for (Supplier s : suppliers) {
            cmbSupplier.addItem(s.getName());
        }
        initDatePicker();
        btnClearBillDetail.setVisible(false);
        btnClearBillDetail.setBackground(Color.decode("#b465c8"));
        btnSave.setBackground(Color.decode("#d82c98"));
        setSize(WIDTH, HEIGHT);
        bill = new Bill();
        bill.setEmployee(EmployeeService.currentEmployee);
        JTableHeader header = tbBillDetail.getTableHeader();
        header.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        tbBillDetail.setModel(new AbstractTableModel() {
            String[] headers = {"รายการ", "ราคาต่อหน่วย", "จำนวน", "ราคารวม"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return bill.getBillItems().size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<BillItem> billItems = bill.getBillItems();
                BillItem billItem = billItems.get(rowIndex);
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
                String formattedValuePrice = decimalFormat.format(billItem.getPrice());
                String formattedValueTotal = decimalFormat.format(billItem.getTotal());
                switch (columnIndex) {
                    case 0:
                        return billItem.getMaterialName();
                    case 1:
                        return formattedValuePrice;
                    case 2:
                        return billItem.getQty();
                    case 3:
                        return formattedValueTotal;
                    default:
                        return "";
                }
            }
// บวกค่าrecirptdetail เล็กสุดใหม่

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<BillItem> billItems = bill.getBillItems();
                BillItem billItem = billItems.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    billItem.setQty(qty);
                    bill.calculateSubTotal();
                    refreshBill();
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }

        });
        materialListPanel.addOnByMaterial(this);
        scrMaterialList.setViewportView(materialListPanel);
    }

    private void refreshBill() {
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
        String formattedValueSubtotal = decimalFormat.format(bill.getSubtotal());
        String formattedValueTotal = decimalFormat.format(bill.getTotal());
        tbBillDetail.revalidate();
        tbBillDetail.repaint();
        txtSubtotal.setText(formattedValueSubtotal);
        txtDiscount.setText("");
        txtTotal.setText(formattedValueTotal);
    }

    private void initMaterialTable() {
        materials = (ArrayList<Material>) materialService.getMaterials();
    }

    private void initDatePicker() {
        model = new UtilDateModel();
        Properties p = new Properties();
        p.put("text.today", "Today");
        p.put("text.month", "Month");
        p.put("text.year", "Year");
        JDatePanelImpl datePanel = new JDatePanelImpl(model, p);
        JDatePickerImpl datePicker = new JDatePickerImpl(datePanel, new DateLabelFormatter());
        pnlDatePicker.add(datePicker);
        model.setSelected(true);
    }

    /**
     * Creates new form POS
     */
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbBillDetail = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtSubtotal = new javax.swing.JLabel();
        txtTotal = new javax.swing.JLabel();
        btnSave = new javax.swing.JButton();
        btnClearBillDetail = new javax.swing.JButton();
        txtDiscount = new javax.swing.JTextField();
        btnCalDiscount = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        pnlDatePicker = new javax.swing.JPanel();
        cmbSupplier = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        scrMaterialList = new javax.swing.JScrollPane();

        jPanel1.setBackground(new java.awt.Color(245, 237, 245));

        jScrollPane3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        tbBillDetail.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbBillDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tbBillDetail);

        jPanel2.setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("ราคารวม");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setText("ส่วนลด");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel3.setText("ราคารวมสุทธิ");

        txtSubtotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtSubtotal.setText("0.0");

        txtTotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtTotal.setText("0.0");

        btnSave.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        btnSave.setForeground(new java.awt.Color(243, 237, 245));
        btnSave.setText("บันทึก");
        btnSave.setBorder(null);
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });

        btnClearBillDetail.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        btnClearBillDetail.setForeground(new java.awt.Color(243, 237, 245));
        btnClearBillDetail.setText("ยกเลิกรายการ");
        btnClearBillDetail.setBorder(null);
        btnClearBillDetail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearBillDetailActionPerformed(evt);
            }
        });

        txtDiscount.setHorizontalAlignment(javax.swing.JTextField.RIGHT);
        txtDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtDiscountActionPerformed(evt);
            }
        });

        btnCalDiscount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCalDiscount.setForeground(new java.awt.Color(255, 255, 255));
        btnCalDiscount.setText("คำนวณ");
        btnCalDiscount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCalDiscountActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTotal, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtSubtotal, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(10, 10, 10))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnCalDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(244, Short.MAX_VALUE)
                .addComponent(btnClearBillDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtSubtotal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnCalDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtDiscount, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(1, 1, 1)
                        .addComponent(txtTotal))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnSave, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClearBillDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel4.setBackground(new java.awt.Color(124, 56, 147));

        pnlDatePicker.setBackground(new java.awt.Color(255, 255, 255));
        pnlDatePicker.setOpaque(false);

        cmbSupplier.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("วันที่ :");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("ร้านค้า :");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(pnlDatePicker, javax.swing.GroupLayout.DEFAULT_SIZE, 211, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cmbSupplier, 0, 182, Short.MAX_VALUE)
                .addGap(23, 23, 23))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pnlDatePicker, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbSupplier, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE))
                .addContainerGap(10, Short.MAX_VALUE))
        );

        scrMaterialList.setOpaque(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(scrMaterialList, javax.swing.GroupLayout.DEFAULT_SIZE, 595, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 508, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addComponent(scrMaterialList)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed
        if (bill.getBillItems().isEmpty()) {
            JOptionPane.showMessageDialog(this, "กรุณาเลือกรายการวัตถุดิบ");
        } else {
            try {
                String pattern = "yyyy-MM-dd";
                SimpleDateFormat formater = new SimpleDateFormat(pattern);

                bill.setForSaveDate(formater.format(model.getValue()) + " " + "00:00:00");
                String selectedItem = (String) cmbSupplier.getSelectedItem();
                Supplier supplier = supplierService.getSupplierByName(selectedItem);
                bill.setSupplier(supplier);
                billService.addNew(bill);
            } catch (ValidateException ex) {
                Logger.getLogger(ImportMaterialPanel.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, ex.getMessage());
                return;
            }
            clearBill();
        }

    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnClearBillDetailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearBillDetailActionPerformed
        bill.delBillItem();
        btnClearBillDetail.setVisible(false);
        clearBill();
    }//GEN-LAST:event_btnClearBillDetailActionPerformed

    private void btnCalDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCalDiscountActionPerformed
        Double discount = Double.valueOf(txtDiscount.getText());
        bill.setDiscount(discount);
        bill.setTotal(bill.getSubtotal() - bill.getDiscount());
        refreshBill();
    }//GEN-LAST:event_btnCalDiscountActionPerformed

    private void txtDiscountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtDiscountActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDiscountActionPerformed

    public void clearBill() {
        bill = new Bill();
        bill.setEmployee(employeeService.getCurrentEmployee());
        refreshBill();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCalDiscount;
    private javax.swing.JButton btnClearBillDetail;
    private javax.swing.JButton btnSave;
    private javax.swing.JComboBox<String> cmbSupplier;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JPanel pnlDatePicker;
    private javax.swing.JScrollPane scrMaterialList;
    private javax.swing.JTable tbBillDetail;
    private javax.swing.JTextField txtDiscount;
    private javax.swing.JLabel txtSubtotal;
    private javax.swing.JLabel txtTotal;
    // End of variables declaration//GEN-END:variables

    @Override
    public void buy(Material material, Double pricePerUnit, int qty) {
        btnClearBillDetail.setVisible(true);
        boolean materialExits = false;
        for (BillItem item : bill.getBillItems()) {
            if (item.getMaterialId() == material.getId()) {
                item.setQty(item.getQty() + qty);
                bill.calculateSubTotal();
                materialExits = true;
                break;
            }
        }
        if (!materialExits) {
            bill.addBillItem(material, pricePerUnit, qty);
        }
        refreshBill();
    }
}
