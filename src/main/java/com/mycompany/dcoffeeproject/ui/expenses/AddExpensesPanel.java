/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.expenses;

import com.mycompany.dcoffeeproject.model.Expense;
import com.mycompany.dcoffeeproject.model.ExpenseItem;
import com.mycompany.dcoffeeproject.model.Product;
import com.mycompany.dcoffeeproject.model.Receipt;
import com.mycompany.dcoffeeproject.service.EmployeeService;
import com.mycompany.dcoffeeproject.service.ExpenseService;
import com.mycompany.dcoffeeproject.service.ProductService;
import com.mycompany.dcoffeeproject.service.ValidateException;
import com.mycompany.dcoffeeproject.ui.pos.FindMemberDialog;
import static com.mycompany.dcoffeeproject.ui.product.ProductPanel.tbProduct;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import static java.awt.image.ImageObserver.HEIGHT;
import static java.awt.image.ImageObserver.WIDTH;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author ส้มส้ม
 */
public class AddExpensesPanel extends javax.swing.JPanel {

    private final AbstractTableModel model;
    private Expense expense;
    private final AbstractTableModel expenseItemModel;
    private String[] expenseList = {"ค่าวัตถุดิบ", "ค่าจ้างพนักงาน", "ค่าเช่า", "ค่าไฟ", "ค่าน้ำ", "อื่นๆ"};
    private ExpenseService expenseService = new ExpenseService();

    /**
     * Creates new form AddExpensesPanel
     */
    public AddExpensesPanel() {
        initComponents();
        expense = new Expense();
        expense.setEmployee(EmployeeService.currentEmployee);
        btnCancel.setBackground(Color.decode("#b465c8"));
        btnSave.setBackground(Color.decode("#b465c8"));
        JTableHeader header = tbExpense.getTableHeader();
        header.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        header.setBackground(Color.decode("#7c3893"));
        header.setForeground(Color.decode("#f3edf5"));

        JTableHeader header2 = tbExpenseItem.getTableHeader();
        header2.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        header2.setBackground(Color.decode("#7c3893"));
        header2.setForeground(Color.decode("#f3edf5"));
        tbExpense.setRowHeight(40);

        btnCancel.setVisible(false);
        model = new AbstractTableModel() {
            String[] columnNames = {"รายการ"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return 6;
            }

            @Override
            public int getColumnCount() {
                return 1;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 0:
                        return expenseList[rowIndex];
                    default:
                        return "Unknown";
                }
            }
        };
        tbExpense.setModel(model);
        expenseItemModel = new AbstractTableModel() {
            String[] columnNames = {"รายการ", "ราคา"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return expense.getExpenseItems().size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<ExpenseItem> expenseItems = expense.getExpenseItems();
                ExpenseItem expenseItem = expenseItems.get(rowIndex);
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
                String formattedValueTotal = decimalFormat.format(expenseItem.getTotal());
                switch (columnIndex) {
                    case 0:
                        return expenseItem.getName();
                    case 1:
                        return formattedValueTotal;
                    default:
                        return "Unknown";
                }
            }
        };
        setSize(WIDTH, HEIGHT);
        tbExpenseItem.setModel(expenseItemModel);
        tbExpense.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int index = tbExpense.getSelectedRow();
                String name = expenseList[index];
                //expense.addExpenseItem(expenseList[index], 0.0);
                if (name.equals("อื่นๆ")) {
                    openOtherExpenseDialog();
                } else {
                    openInputTotalDialog(name);
                }
            }
        });

    }

    private void refreshExpenseItem() {
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
        String formattedValue = decimalFormat.format(expense.getTotal());
        tbExpenseItem.revalidate();
        tbExpenseItem.repaint();
        lblTotal.setText(formattedValue);
    }

    private void openInputTotalDialog(String name) {
        //JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        InputTotalDialog inputTotalDialog = new InputTotalDialog(this, expense, name);
        inputTotalDialog.setLocationRelativeTo(this);
        inputTotalDialog.setVisible(true);
        inputTotalDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshExpenseItem();
            }
        });
    }

    private void openOtherExpenseDialog() {
        //JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        OtherExpenseDialog otherExpenseDialog = new OtherExpenseDialog(this, expense);
        otherExpenseDialog.setLocationRelativeTo(this);
        otherExpenseDialog.setVisible(true);
        otherExpenseDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshExpenseItem();
            }
        });
    }

    public Expense getExpense() {
        return expense;
    }

    public void setExpense(Expense expense) {
        this.expense = expense;
    }

    public void clearExpense() {
        expense = new Expense();
        expense.setEmployee(EmployeeService.currentEmployee);
        btnCancel.setVisible(false);
        refreshExpenseItem();

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tbExpense = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbExpenseItem = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnCancel = new javax.swing.JButton();
        btnSave = new javax.swing.JButton();

        setBackground(new java.awt.Color(245, 237, 245));

        tbExpense.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbExpense.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbExpense);

        tbExpenseItem.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbExpenseItem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tbExpenseItem);

        jPanel1.setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("ราคารวม");

        lblTotal.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lblTotal.setText("0.0");

        jPanel2.setLayout(new java.awt.GridLayout(1, 0, 10, 0));

        btnCancel.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnCancel.setForeground(new java.awt.Color(243, 237, 245));
        btnCancel.setText("ยกเลิกรายการ");
        btnCancel.setPreferredSize(new java.awt.Dimension(200, 35));
        btnCancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelActionPerformed(evt);
            }
        });
        jPanel2.add(btnCancel);

        btnSave.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btnSave.setForeground(new java.awt.Color(243, 237, 245));
        btnSave.setText("บันทึก");
        btnSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSaveActionPerformed(evt);
            }
        });
        jPanel2.add(btnSave);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 352, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblTotal)))
                .addGap(10, 10, 10))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(lblTotal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 21, Short.MAX_VALUE)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 427, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 570, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 499, Short.MAX_VALUE)
                        .addGap(12, 12, 12)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSaveActionPerformed

        if (expense.getExpenseItems().isEmpty()) {
            JOptionPane.showMessageDialog(this, "กรุณาเลือกรายการ");
        } else {
            try {
                expenseService.addNew(expense);
            } catch (ValidateException ex) {
                Logger.getLogger(AddExpensesPanel.class.getName()).log(Level.SEVERE, null, ex);
                JOptionPane.showMessageDialog(this, ex.getMessage());
                return;
            }
            clearExpense();
        }
    }//GEN-LAST:event_btnSaveActionPerformed

    private void btnCancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelActionPerformed
        clearExpense();
    }//GEN-LAST:event_btnCancelActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    public javax.swing.JButton btnCancel;
    private javax.swing.JButton btnSave;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JTable tbExpense;
    private javax.swing.JTable tbExpenseItem;
    // End of variables declaration//GEN-END:variables
}
