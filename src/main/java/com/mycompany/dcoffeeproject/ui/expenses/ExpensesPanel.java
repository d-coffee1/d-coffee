/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.expenses;

import com.mycompany.dcoffeeproject.DateLabelFormatter;
import com.mycompany.dcoffeeproject.model.CheckMaterial;
import com.mycompany.dcoffeeproject.model.CheckMaterialItem;
import com.mycompany.dcoffeeproject.model.Expense;
import com.mycompany.dcoffeeproject.model.ExpenseItem;
import com.mycompany.dcoffeeproject.service.ExpenseService;
import com.mycompany.dcoffeeproject.ui.MainFrame;
import java.awt.Color;
import java.awt.Font;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import static javax.management.Query.value;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author ส้มส้ม
 */
public class ExpensesPanel extends javax.swing.JPanel {

    private final AbstractTableModel expenseModel;
    private ExpenseService expenseService = new ExpenseService();
    private List<Expense> expenses;
    private List<ExpenseItem> expenseItems;
    private final AbstractTableModel expenseItemModel;
    private UtilDateModel startDateModel;
    private UtilDateModel endDateModel;

    /**
     * Creates new form ExpensesPanel
     */
    public ExpensesPanel() {
        initComponents();
        JTableHeader header = tbExpense.getTableHeader();
        header.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        JTableHeader header2 = tbExpensesItem.getTableHeader();
        header2.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        header.setBackground(Color.decode("#7c3893"));
        header.setForeground(Color.decode("#f3edf5"));
        header2.setBackground(Color.decode("#7c3893"));
        header2.setForeground(Color.decode("#f3edf5"));
        expenses = expenseService.getExpenses();
        initDatePicker();
        expenseModel = new AbstractTableModel() {
            String[] columnNames = {"วันที่", "ชื่อ -  นามสกุล", "ยอดรวมสุทธิ"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return expenses.size();
            }

            @Override
            public int getColumnCount() {
                return 3;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Expense expense = expenses.get(rowIndex);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
                String formattedValue = decimalFormat.format(expense.getTotal());
                String expenseDate = dateFormat.format(expense.getDate());
                switch (columnIndex) {
                    case 0:
                        return expenseDate;
                    case 1:
                        return expense.getEmployee().getName() + " " + expense.getEmployee().getSurname();
                    case 2:
                        return formattedValue;
                    default:
                        return "Unknown";
                }
            }
        };
        tbExpense.setModel(expenseModel);
        expenseItemModel = new AbstractTableModel() {
            String[] columnNames = {"รายการ", "ยอดรวม"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return expenseItems == null ? 0 : expenseItems.size();
            }

            @Override
            public int getColumnCount() {
                return 2;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ExpenseItem expenseItem = expenseItems.get(rowIndex);
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
                String formattedValue = decimalFormat.format(expenseItem.getTotal());
                switch (columnIndex) {
                    case 0:
                        return expenseItem == null ? "" : expenseItem.getName();
                    case 1:
                        return expenseItem == null ? "" : formattedValue;
                    default:
                        return "Unknown";
                }
            }
        };
        tbExpensesItem.setModel(expenseItemModel);

    }

    private void initDatePicker() {
        Font tahomaFont = new Font("Tahoma", Font.PLAIN, 12);
        startDateModel = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("text.today", "วัน");
        p1.put("text.month", "เดือน");
        p1.put("text.year", "ปี");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(startDateModel, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
        pnlStartDatePicker.add(datePicker1);

        endDateModel = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("text.today", "วัน");
        p2.put("text.month", "เดือน");
        p2.put("text.year", "ปี");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(endDateModel, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        pnlEndDatePicker.add(datePicker2);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tbExpense = new javax.swing.JTable();
        btnAdd = new javax.swing.JButton();
        btnViewItem = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        pnlStartDatePicker = new javax.swing.JPanel();
        pnlEndDatePicker = new javax.swing.JPanel();
        btnSearch = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbExpensesItem = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lbTotal = new javax.swing.JLabel();

        setBackground(new java.awt.Color(243, 237, 245));

        tbExpense.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbExpense.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbExpense);

        btnAdd.setBackground(new java.awt.Color(124, 56, 147));
        btnAdd.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnAdd.setForeground(new java.awt.Color(243, 237, 245));
        btnAdd.setText("เพิ่มรายจ่าย");
        btnAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAddActionPerformed(evt);
            }
        });

        btnViewItem.setBackground(new java.awt.Color(124, 56, 147));
        btnViewItem.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnViewItem.setForeground(new java.awt.Color(243, 237, 245));
        btnViewItem.setText("ดูรายละเอียด");
        btnViewItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewItemActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("รายจ่าย");

        pnlStartDatePicker.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        pnlStartDatePicker.setOpaque(false);

        pnlEndDatePicker.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        pnlEndDatePicker.setOpaque(false);

        btnSearch.setBackground(new java.awt.Color(124, 56, 147));
        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(243, 237, 245));
        btnSearch.setText("ค้นหา");
        btnSearch.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        jScrollPane2.setOpaque(false);

        tbExpensesItem.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbExpensesItem.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tbExpensesItem);

        jPanel1.setBackground(new java.awt.Color(243, 237, 245));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setText("ยอดรวมสุทธิ");

        lbTotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbTotal.setText("0.0");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbTotal)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lbTotal))
                .addContainerGap(180, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(pnlStartDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(pnlEndDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnViewItem, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(btnAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 784, Short.MAX_VALUE)
                            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(19, 19, 19)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(pnlEndDatePicker, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlStartDatePicker, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnSearch, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                        .addComponent(btnViewItem, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                        .addComponent(btnAdd, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 792, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 571, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewItemActionPerformed
        int index = tbExpense.getSelectedRow();
        if (index >= 0) {
            Expense expense = expenses.get(index);
            expenseItems = expense.getExpenseItems();
            DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
            String formattedValueTotal = decimalFormat.format(expense.getTotal());
            lbTotal.setText(formattedValueTotal);
        }
        refreshTable();
    }//GEN-LAST:event_btnViewItemActionPerformed

    private void btnAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAddActionPerformed
        MainFrame mainFrame = (MainFrame) SwingUtilities.getWindowAncestor(this);
        mainFrame.scrPanel.setViewportView(new AddExpensesPanel());
    }//GEN-LAST:event_btnAddActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        if (startDateModel.getValue() != null && endDateModel.getValue() == null) {
            JOptionPane.showMessageDialog(this, "กรุณาเลือกวันสิ้นสุด");
        } else if (startDateModel.getValue() == null && endDateModel.getValue() != null) {
            JOptionPane.showMessageDialog(this, "กรุณาเลือกวันเริ่มต้น");
        } else if (startDateModel.getValue() == null && endDateModel.getValue() == null) {
            expenses = expenseService.getExpenses();
        } else if (startDateModel.getValue() != null && endDateModel.getValue() != null) {
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            String start = formater.format(startDateModel.getValue());
            String end = formater.format(endDateModel.getValue());
            expenses = expenseService.getExpenses(start, end);
        }
        refreshTable();
    }//GEN-LAST:event_btnSearchActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAdd;
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnViewItem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lbTotal;
    private javax.swing.JPanel pnlEndDatePicker;
    private javax.swing.JPanel pnlStartDatePicker;
    private javax.swing.JTable tbExpense;
    private javax.swing.JTable tbExpensesItem;
    // End of variables declaration//GEN-END:variables

    private void refreshTable() {
        expenseModel.fireTableDataChanged();
        expenseItemModel.fireTableDataChanged();
    }
}
