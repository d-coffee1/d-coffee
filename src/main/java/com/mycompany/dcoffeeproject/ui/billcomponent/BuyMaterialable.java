/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.billcomponent;

import com.mycompany.dcoffeeproject.model.Material;



/**
 *
 * @author ส้มส้ม
 */
public interface BuyMaterialable {
    public void buy(Material material,Double pricePerUnit,int qty);
}
