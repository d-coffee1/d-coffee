/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.employee;

import com.mycompany.dcoffeeproject.DateLabelFormatter;
import com.mycompany.dcoffeeproject.model.CheckInOut;
import com.mycompany.dcoffeeproject.model.Employee;
import com.mycompany.dcoffeeproject.service.CheckInOutService;
import com.mycompany.dcoffeeproject.service.EmployeeService;
import com.mycompany.dcoffeeproject.ui.MainFrame;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author ส้มส้ม
 */
public class CheckInOutHistoryPanel extends javax.swing.JPanel {

    private final AbstractTableModel model;
    private List<CheckInOut> checkInOuts;
    private CheckInOutService checkInOutService = new CheckInOutService();
    private EmployeeService employeeService = new EmployeeService();
    private UtilDateModel startDateModel;
    private UtilDateModel endDateModel;

    public CheckInOutHistoryPanel() {
        initComponents();
        JTableHeader header = tbCheckInOutHistory.getTableHeader();
        header.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        header.setBackground(Color.decode("#7c3893"));
        header.setForeground(Color.decode("#f3edf5"));
        cmbEmp.addItem("ทั้งหมด");
        List<Employee> employees = employeeService.getEmployees();
        for (Employee e : employees) {
            cmbEmp.addItem(e.getId() + " " + e.getName() + " " + e.getPassword());
        }
        initDatePicker();
        checkInOuts = checkInOutService.getCheckInOutsHistory();
        model = new AbstractTableModel() {
            String[] columnNames = {"วันที่", "รหัสพนักงาน", "ชื่อ-นามสกุล", "เวลาเข้างาน", "เวลาออกงาน", "เวลาทำงาน", "สถานะการจ่ายเงิน"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return checkInOuts.size();
            }

            @Override
            public int getColumnCount() {
                return 7;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                CheckInOut checkInOut = checkInOuts.get(rowIndex);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
                SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
                String checkIndate = dateFormat.format(checkInOut.getDate());
                String checkInTime = timeFormat.format(checkInOut.getCheckInTime());
                String checkOutTime = timeFormat.format(checkInOut.getCheckOutTime());

                switch (columnIndex) {
                    case 0:
                        return checkIndate;
                    case 1:
                        return checkInOut.getEmployee().getId();
                    case 2:
                        return checkInOut.getEmployee().getName() + " " + checkInOut.getEmployee().getSurname();
                    case 3:
                        return checkInTime;
                    case 4:
                        return checkOutTime;
                    case 5:
                        return checkInOut.getWorkHours();
                    case 6:
                        return checkInOut.getSalaryId() > 0 ? "จ่ายเงินแล้ว (หมายเลขใบเสร็จ" + checkInOut.getSalaryId() + ")" : "ยังไม่จ่ายเงิน";
                    default:
                        return "Unknown";
                }
            }
        };
        tbCheckInOutHistory.setModel(model);
        setSize(WIDTH, HEIGHT);
    }

    private void initDatePicker() {
        Font tahomaFont = new Font("Tahoma", Font.PLAIN, 12);
        startDateModel = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("text.today", "วัน");
        p1.put("text.month", "เดือน");
        p1.put("text.year", "ปี");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(startDateModel, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
        pnlStartDatePicker.add(datePicker1);

        endDateModel = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("text.today", "วัน");
        p2.put("text.month", "เดือน");
        p2.put("text.year", "ปี");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(endDateModel, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        pnlEndDatePicker.add(datePicker2);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tbCheckInOutHistory = new javax.swing.JTable();
        lbCheckInOutHistory = new javax.swing.JLabel();
        btnFindEmp1 = new javax.swing.JButton();
        btnCheckInOut = new javax.swing.JButton();
        pnlEndDatePicker = new javax.swing.JPanel();
        pnlStartDatePicker = new javax.swing.JPanel();
        cmbEmp = new javax.swing.JComboBox<>();

        setBackground(new java.awt.Color(245, 237, 245));
        setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        setPreferredSize(new java.awt.Dimension(1357, 683));

        tbCheckInOutHistory.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbCheckInOutHistory.setForeground(new java.awt.Color(52, 52, 52));
        tbCheckInOutHistory.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbCheckInOutHistory);

        lbCheckInOutHistory.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lbCheckInOutHistory.setForeground(new java.awt.Color(52, 52, 52));
        lbCheckInOutHistory.setText("ประวัติการเข้า-ออกงาน");

        btnFindEmp1.setBackground(new java.awt.Color(124, 56, 147));
        btnFindEmp1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnFindEmp1.setForeground(new java.awt.Color(243, 237, 245));
        btnFindEmp1.setText("ค้นหา");
        btnFindEmp1.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnFindEmp1.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnFindEmp1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFindEmp1ActionPerformed(evt);
            }
        });

        btnCheckInOut.setBackground(new java.awt.Color(124, 56, 147));
        btnCheckInOut.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCheckInOut.setForeground(new java.awt.Color(243, 237, 245));
        btnCheckInOut.setText("ลงชื่อเข้า-ออกงาน");
        btnCheckInOut.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCheckInOutActionPerformed(evt);
            }
        });

        pnlEndDatePicker.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        pnlEndDatePicker.setOpaque(false);

        pnlStartDatePicker.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        pnlStartDatePicker.setOpaque(false);

        cmbEmp.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lbCheckInOutHistory)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, 775, Short.MAX_VALUE)
                        .addComponent(btnCheckInOut, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(pnlStartDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pnlEndDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cmbEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnFindEmp1, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(26, 26, 26)
                .addComponent(lbCheckInOutHistory)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(cmbEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(pnlEndDatePicker, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                            .addComponent(pnlStartDatePicker, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                            .addComponent(btnFindEmp1, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(10, 10, 10)))
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 539, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCheckInOut, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCheckInOutActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCheckInOutActionPerformed
        MainFrame mainFrame = (MainFrame) SwingUtilities.getWindowAncestor(this);
        mainFrame.scrPanel.setViewportView(new CheckInOutPanel());
    }//GEN-LAST:event_btnCheckInOutActionPerformed

    private void btnFindEmp1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFindEmp1ActionPerformed
        if (startDateModel.getValue() != null && endDateModel.getValue() == null) {
            JOptionPane.showMessageDialog(this, "กรุณาเลือกวันสิ้นสุด");
        } else if (startDateModel.getValue() == null && endDateModel.getValue() != null) {
            JOptionPane.showMessageDialog(this, "กรุณาเลือกวันเริ่มต้น");
        } else if (startDateModel.getValue() == null && endDateModel.getValue() == null) {
            if (cmbEmp.getSelectedItem().toString().equals("ทั้งหมด")) {
                checkInOuts = checkInOutService.getCheckInOutsHistory();
            } else {
                String item = cmbEmp.getSelectedItem().toString();
                String[] parts = item.split(" ");
                if (parts.length > 0) {
                    int id = Integer.parseInt(parts[0]);
                    System.out.println(id);
                    checkInOuts = checkInOutService.getCheckInOutsHistory(id);
                }
            }
        } else if (startDateModel.getValue() != null && endDateModel.getValue() != null) {
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            String start = formater.format(startDateModel.getValue());
            String end = formater.format(endDateModel.getValue());
            if(cmbEmp.getSelectedItem().toString().equals("ทั้งหมด")){
                checkInOuts = checkInOutService.getCheckInOutsHistory(start, end);
            }else{
                String item = cmbEmp.getSelectedItem().toString();
                String[] parts = item.split(" ");
                if (parts.length > 0) {
                    int id = Integer.parseInt(parts[0]);
                    checkInOuts = checkInOutService.getCheckInOutsHistory(start, end, id);
                }
            }         
        }
        refreshTable();
    }//GEN-LAST:event_btnFindEmp1ActionPerformed

    public void refreshTable() {
        model.fireTableDataChanged();
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCheckInOut;
    private javax.swing.JButton btnFindEmp1;
    private javax.swing.JComboBox<String> cmbEmp;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbCheckInOutHistory;
    private javax.swing.JPanel pnlEndDatePicker;
    private javax.swing.JPanel pnlStartDatePicker;
    private javax.swing.JTable tbCheckInOutHistory;
    // End of variables declaration//GEN-END:variables
}
