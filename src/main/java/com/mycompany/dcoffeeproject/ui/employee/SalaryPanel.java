/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.employee;

import com.mycompany.dcoffeeproject.DateLabelFormatter;
import com.mycompany.dcoffeeproject.model.Employee;
import com.mycompany.dcoffeeproject.model.Salary;
import com.mycompany.dcoffeeproject.service.EmployeeService;
import com.mycompany.dcoffeeproject.service.SalaryService;
import com.mycompany.dcoffeeproject.ui.MainFrame;
import java.awt.Color;
import java.awt.Font;
import static java.awt.image.ImageObserver.HEIGHT;
import static java.awt.image.ImageObserver.WIDTH;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author thanc
 */
public class SalaryPanel extends javax.swing.JPanel {

    private final AbstractTableModel model;
    private final SalaryService salaryService = new SalaryService();
    private List<Salary> salaries;
    private UtilDateModel startDateModel;
    private UtilDateModel endDateModel;
    private EmployeeService employeeService = new EmployeeService();
    /**
     * Creates new form SalaryPanel
     */
    public SalaryPanel() {
        initComponents();
        JTableHeader header = tbSalary.getTableHeader();
        header.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        header.setBackground(Color.decode("#7c3893"));
        header.setForeground(Color.decode("#f3edf5"));
        salaries = salaryService.getSalaries();
        cmbEmp.addItem("ทั้งหมด");
        List<Employee> employees = employeeService.getEmployees();
        for (Employee e : employees) {
            cmbEmp.addItem(e.getId() + " " + e.getName() + " " + e.getPassword());
        }
        initDatePicker();
        model = new AbstractTableModel() {
            String[] columnNames = {"วันที่", "รหัสพนักงาน", "ชื่อ-นามสกุล","ค่าจ้างต่อชั่วโมง", "จำนวนชั่วโมงการทำงาน", "เงินรวม"};
            
            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return salaries.size();
            }

            @Override
            public int getColumnCount() {
                return 6;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Salary salary = salaries.get(rowIndex);
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
                String formattedValue = decimalFormat.format(salary.getTotalSalary());
                String salarytDate = dateFormat.format(salary.getDate());
                switch (columnIndex) {
                    case 0:
                        return salarytDate;
                    case 1:
                        return salary.getEmployeeId();
                    case 2:
                        return salary.getEmployeeName()+" "+salary.getEmployeeSurname();
                    case 3:
                        return salary.getEmployeeHourWage();
                    case 4:
                        return salary.getTotalWorkHours();
                    case 5:
                        return formattedValue;
                    default:
                        return "Unknown";
                }
            }
        };
        tbSalary.setModel(model);
        setSize(WIDTH, HEIGHT);
    }
    
      private void initDatePicker() {
        Font tahomaFont = new Font("Tahoma", Font.PLAIN, 12);
        startDateModel = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("text.today", "วัน");
        p1.put("text.month", "เดือน");
        p1.put("text.year", "ปี");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(startDateModel, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
        pnlStartDatePicker.add(datePicker1);

        endDateModel = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("text.today", "วัน");
        p2.put("text.month", "เดือน");
        p2.put("text.year", "ปี");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(endDateModel, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        pnlEndDatePicker.add(datePicker2);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        lbCheckInOutHistory = new javax.swing.JLabel();
        btnPaySalary = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbSalary = new javax.swing.JTable();
        pnlStartDatePicker = new javax.swing.JPanel();
        pnlEndDatePicker = new javax.swing.JPanel();
        cmbEmp = new javax.swing.JComboBox<>();
        btnSearch = new javax.swing.JButton();

        jPanel1.setBackground(new java.awt.Color(245, 237, 245));

        lbCheckInOutHistory.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        lbCheckInOutHistory.setForeground(new java.awt.Color(52, 52, 52));
        lbCheckInOutHistory.setText("จ่ายเงินเดือนพนักงาน");

        btnPaySalary.setBackground(new java.awt.Color(124, 56, 147));
        btnPaySalary.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnPaySalary.setForeground(new java.awt.Color(243, 237, 245));
        btnPaySalary.setText("จ่ายเงินเดือนพนักงาน");
        btnPaySalary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPaySalaryActionPerformed(evt);
            }
        });

        tbSalary.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbSalary.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbSalary);

        pnlStartDatePicker.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        pnlStartDatePicker.setOpaque(false);

        pnlEndDatePicker.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        pnlEndDatePicker.setOpaque(false);

        cmbEmp.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        btnSearch.setBackground(new java.awt.Color(124, 56, 147));
        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(243, 237, 245));
        btnSearch.setText("ค้นหา");
        btnSearch.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSearch.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1177, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lbCheckInOutHistory)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(pnlStartDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(pnlEndDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(cmbEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 273, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnPaySalary, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(lbCheckInOutHistory)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(pnlEndDatePicker, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(pnlStartDatePicker, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btnSearch, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnPaySalary, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 630, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnPaySalaryActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPaySalaryActionPerformed
        MainFrame mainFrame = (MainFrame) SwingUtilities.getWindowAncestor(this);
        mainFrame.scrPanel.setViewportView(new PaySalaryPanel());
    }//GEN-LAST:event_btnPaySalaryActionPerformed

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        if (startDateModel.getValue() != null && endDateModel.getValue() == null) {
            JOptionPane.showMessageDialog(this, "กรุณาเลือกวันสิ้นสุด");
        } else if (startDateModel.getValue() == null && endDateModel.getValue() != null) {
            JOptionPane.showMessageDialog(this, "กรุณาเลือกวันเริ่มต้น");
        } else if (startDateModel.getValue() == null && endDateModel.getValue() == null) {
            if (cmbEmp.getSelectedItem().toString().equals("ทั้งหมด")) {
                salaries = salaryService.getSalaries();
            } else {
                String item = cmbEmp.getSelectedItem().toString();
                String[] parts = item.split(" ");
                if (parts.length > 0) {
                    int id = Integer.parseInt(parts[0]);
                    System.out.println(id);
                    salaries = salaryService.getSalaries(id);
                }
            }
        } else if (startDateModel.getValue() != null && endDateModel.getValue() != null) {
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            String start = formater.format(startDateModel.getValue());
            String end = formater.format(endDateModel.getValue());
            if(cmbEmp.getSelectedItem().toString().equals("ทั้งหมด")){
                salaries = salaryService.getSalaries(start,end);
            }else{
                String item = cmbEmp.getSelectedItem().toString();
                String[] parts = item.split(" ");
                if (parts.length > 0) {
                    int id = Integer.parseInt(parts[0]);
                    salaries = salaryService.getSalaries(start,end,id);
                }
            }
        }
        refreshTable();
    }//GEN-LAST:event_btnSearchActionPerformed

    

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnPaySalary;
    private javax.swing.JButton btnSearch;
    private javax.swing.JComboBox<String> cmbEmp;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbCheckInOutHistory;
    private javax.swing.JPanel pnlEndDatePicker;
    private javax.swing.JPanel pnlStartDatePicker;
    private javax.swing.JTable tbSalary;
    // End of variables declaration//GEN-END:variables

    private void refreshTable() {
        model.fireTableDataChanged();
    }
}
