/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.receipt;

import com.mycompany.dcoffeeproject.DateLabelFormatter;
import com.mycompany.dcoffeeproject.model.CheckMaterialItem;
import com.mycompany.dcoffeeproject.model.Promotion;
import com.mycompany.dcoffeeproject.model.Receipt;
import com.mycompany.dcoffeeproject.model.ReceiptDetail;
import com.mycompany.dcoffeeproject.service.PromotionService;
import com.mycompany.dcoffeeproject.service.ReceiptService;
import java.awt.Color;
import java.awt.Font;
import static java.awt.image.ImageObserver.HEIGHT;
import static java.awt.image.ImageObserver.WIDTH;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Properties;
import static javax.management.Query.value;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import org.jdatepicker.impl.JDatePanelImpl;
import org.jdatepicker.impl.JDatePickerImpl;
import org.jdatepicker.impl.UtilDateModel;

/**
 *
 * @author ส้มส้ม
 */
public class ReceiptPanel extends javax.swing.JPanel {

    private List<ReceiptDetail> receiptDetails;
    private List<Receipt> receipts;
    private final AbstractTableModel receiptDetailModel;
    private final AbstractTableModel receiptModel;
    private ReceiptService receiptService = new ReceiptService();
    private PromotionService promotionService = new PromotionService();
    private UtilDateModel startDateModel;
    private UtilDateModel endDateModel;

    /**
     * Creates new form ReceiptPanel
     */
    public ReceiptPanel() {
        initComponents();
        JTableHeader headerCheckMat = tbReceipt.getTableHeader();
        headerCheckMat.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        headerCheckMat.setBackground(Color.decode("#7c3893"));
        headerCheckMat.setForeground(Color.decode("#f3edf5"));

        JTableHeader headerCheckMatItem = tbReceiptDetail.getTableHeader();
        headerCheckMatItem.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        headerCheckMatItem.setBackground(Color.decode("#7c3893"));
        headerCheckMatItem.setForeground(Color.decode("#f3edf5"));
        setSize(WIDTH, HEIGHT);
        initDatePicker();
        receipts = receiptService.getReciepts();
        receiptModel = new AbstractTableModel() {
            String[] columnNames = {"วันที่ - เวลา", "พนักงาน", "สมาชิก", "ราคารวม"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return receipts.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                Receipt receipt = receipts.get(rowIndex);
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
                String formattedValueTotal = decimalFormat.format(receipt.getTotal());
                SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                String receiptDate = dateFormat.format(receipt.getCreatedDate());
                switch (columnIndex) {
                    case 0:
                        return receiptDate;
                    case 1:
                        return receipt.getEmployee().getName() + " " + receipt.getEmployee().getSurname();
                    case 2:
                        return receipt.getMember() == null ? "-" : receipt.getMember().getName() + " " + receipt.getMember().getSurname();
                    case 3:
                        return formattedValueTotal;
                    default:
                        return "Unknown";
                }
            }
        };
        tbReceipt.setModel(receiptModel);
        receiptDetailModel = new AbstractTableModel() {
            String[] columnNames = {"รายการ", "ราคา", "จำนวน", "ราคารวมสุทธิ"};

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return receiptDetails == null ? 0 : receiptDetails.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReceiptDetail receiptDetail = receiptDetails.get(rowIndex);
                DecimalFormat decimalFormat = new DecimalFormat("#,###");
                DecimalFormat decimalFormat2 = new DecimalFormat("#,##0.0");
                String formattedValueTotalQty = decimalFormat.format(receiptDetail.getQty());
                String formattedValueTotalPrice = decimalFormat2.format(receiptDetail.getTotalPrice());
                switch (columnIndex) {
                    case 0:
                        return receiptDetail == null ? "" : receiptDetail.getName();
                    case 1:
                        return receiptDetail == null ? "" : receiptDetail.getPrice();
                    case 2:
                        return receiptDetail == null ? "" : formattedValueTotalQty;
                    case 3:
                        return receiptDetail == null ? "" : formattedValueTotalPrice;
                    default:
                        return "Unknown";
                }
            }
        };
        tbReceiptDetail.setModel(receiptDetailModel);
    }

    private void initDatePicker() {
        Font tahomaFont = new Font("Tahoma", Font.PLAIN, 12);
        startDateModel = new UtilDateModel();
        Properties p1 = new Properties();
        p1.put("text.today", "วัน");
        p1.put("text.month", "เดือน");
        p1.put("text.year", "ปี");
        JDatePanelImpl datePanel1 = new JDatePanelImpl(startDateModel, p1);
        JDatePickerImpl datePicker1 = new JDatePickerImpl(datePanel1, new DateLabelFormatter());
        pnlStartDatePicker.add(datePicker1);

        endDateModel = new UtilDateModel();
        Properties p2 = new Properties();
        p2.put("text.today", "วัน");
        p2.put("text.month", "เดือน");
        p2.put("text.year", "ปี");
        JDatePanelImpl datePanel2 = new JDatePanelImpl(endDateModel, p2);
        JDatePickerImpl datePicker2 = new JDatePickerImpl(datePanel2, new DateLabelFormatter());
        pnlEndDatePicker.add(datePicker2);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        btnViewItem = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbReceipt = new javax.swing.JTable();
        pnlEndDatePicker = new javax.swing.JPanel();
        pnlStartDatePicker = new javax.swing.JPanel();
        btnSearch = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbReceiptDetail = new javax.swing.JTable();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lblSubTotal = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblChange = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        lblReceive = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblPaymentMethod = new javax.swing.JLabel();
        lblPromotion = new javax.swing.JLabel();
        lblID = new javax.swing.JLabel();
        lblPaymentMethod1 = new javax.swing.JLabel();

        setBackground(new java.awt.Color(243, 237, 245));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 36)); // NOI18N
        jLabel1.setText("รายการใบเสร็จ");
        jLabel1.setPreferredSize(new java.awt.Dimension(267, 29));

        btnViewItem.setBackground(new java.awt.Color(124, 56, 147));
        btnViewItem.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnViewItem.setForeground(new java.awt.Color(243, 237, 245));
        btnViewItem.setText("ดูรายละเอียด");
        btnViewItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewItemActionPerformed(evt);
            }
        });

        tbReceipt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbReceipt.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane2.setViewportView(tbReceipt);

        pnlEndDatePicker.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        pnlEndDatePicker.setOpaque(false);

        pnlStartDatePicker.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        pnlStartDatePicker.setOpaque(false);

        btnSearch.setBackground(new java.awt.Color(124, 56, 147));
        btnSearch.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnSearch.setForeground(new java.awt.Color(243, 237, 245));
        btnSearch.setText("ค้นหา");
        btnSearch.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnSearch.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSearchActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(124, 56, 147)));
        jPanel3.setOpaque(false);

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel7.setText("รายละเอียดใบเสร็จ");

        tbReceiptDetail.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbReceiptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbReceiptDetail);

        jPanel1.setOpaque(false);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel4.setText("วิธีการชำระเงิน");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setText("ราคารวม");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel3.setText("โปรโมชั่น");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel5.setText("ส่วนลด");

        lblSubTotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblSubTotal.setText("0.0");

        lblDiscount.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblDiscount.setText("0.0");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel9.setText("เงินทอน");

        lblChange.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblChange.setText("0.0");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel8.setText("เงินที่รับมา");

        lblReceive.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblReceive.setText("0.0");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel6.setText("ราคารวมสุทธิ");

        lblTotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblTotal.setText("0.0");

        lblPaymentMethod.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblPaymentMethod.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        lblPromotion.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblPromotion.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblChange))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addComponent(jLabel8))
                        .addGap(0, 528, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblReceive, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(lblTotal, javax.swing.GroupLayout.Alignment.TRAILING)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblSubTotal))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblDiscount))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblPaymentMethod, javax.swing.GroupLayout.DEFAULT_SIZE, 436, Short.MAX_VALUE)
                            .addComponent(lblPromotion, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(lblSubTotal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel3)
                    .addComponent(lblPromotion, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 12, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(lblDiscount))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(lblTotal))
                        .addGap(28, 28, 28)
                        .addComponent(jLabel4))
                    .addComponent(lblPaymentMethod, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblReceive)
                    .addComponent(jLabel8))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblChange)
                    .addComponent(jLabel9))
                .addGap(18, 18, 18))
        );

        lblID.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblID.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel7)
                .addGap(26, 26, 26)
                .addComponent(lblID, javax.swing.GroupLayout.PREFERRED_SIZE, 55, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(244, 244, 244))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lblID, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 496, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        lblPaymentMethod1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblPaymentMethod1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(746, 746, 746)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(pnlStartDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(pnlEndDatePicker, javax.swing.GroupLayout.PREFERRED_SIZE, 211, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnSearch, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnViewItem, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 431, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 733, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(731, Short.MAX_VALUE)))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(517, 517, 517)
                    .addComponent(lblPaymentMethod1, javax.swing.GroupLayout.DEFAULT_SIZE, 436, Short.MAX_VALUE)
                    .addGap(517, 517, 517)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(57, 57, 57)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(pnlEndDatePicker, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pnlStartDatePicker, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                    .addComponent(btnSearch, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btnViewItem, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(58, 58, 58)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 831, Short.MAX_VALUE)
                    .addContainerGap()))
            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                    .addGap(459, 459, 459)
                    .addComponent(lblPaymentMethod1, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(454, Short.MAX_VALUE)))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewItemActionPerformed
        int index = tbReceipt.getSelectedRow();
        Receipt receipt = receipts.get(index);
        if (index >= 0) {
            receiptDetails = receipts.get(index).getReceiptDetails();
        }
        showReceiptDetail(receipt);
        refreshTable();
    }//GEN-LAST:event_btnViewItemActionPerformed

    private void showReceiptDetail(Receipt receipt) {
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
        String formattedValueTotalSubtotal = decimalFormat.format(receipt.getSubtotal());
        String formattedValueTotalDiscount = decimalFormat.format(receipt.getDiscount());
        String formattedValueTotal = decimalFormat.format(receipt.getTotal());
        String formattedValueReceipt = decimalFormat.format(receipt.getReceive());
        String formattedValueChange = decimalFormat.format(receipt.getChange());
        lblID.setText(receipt.getId() + "");
        Promotion pro = new Promotion();
        lblPaymentMethod.setText(receipt.getPaymentMethod());
        lblSubTotal.setText(formattedValueTotalSubtotal);
        if (receipt.getPromotionId() > 0) {
            pro = promotionService.getOne(receipt.getPromotionId());
            lblPromotion.setText("" + pro.getName());
        } else {
            lblPromotion.setText("ไม่มี");
        }
        lblDiscount.setText(formattedValueTotalDiscount);
        lblTotal.setText(formattedValueTotal);
        lblReceive.setText(formattedValueReceipt);
        double change = receipt.getReceive() - receipt.getTotal();
        lblChange.setText(formattedValueChange);

    }

    private void btnSearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSearchActionPerformed
        if (startDateModel.getValue() != null && endDateModel.getValue() == null) {
            JOptionPane.showMessageDialog(this, "กรุณาเลือกวันสิ้นสุด");
        } else if (startDateModel.getValue() == null && endDateModel.getValue() != null) {
            JOptionPane.showMessageDialog(this, "กรุณาเลือกวันเริ่มต้น");
        } else if (startDateModel.getValue() == null && endDateModel.getValue() == null) {
            receipts = receiptService.getReciepts();
        } else if (startDateModel.getValue() != null && endDateModel.getValue() != null) {
            SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd");
            String start = formater.format(startDateModel.getValue());
            String end = formater.format(endDateModel.getValue());
            receipts = receiptService.getReciepts(start, end);
        }
        refreshTable();
    }//GEN-LAST:event_btnSearchActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnSearch;
    private javax.swing.JButton btnViewItem;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel lblChange;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblID;
    private javax.swing.JLabel lblPaymentMethod;
    private javax.swing.JLabel lblPaymentMethod1;
    private javax.swing.JLabel lblPromotion;
    private javax.swing.JLabel lblReceive;
    private javax.swing.JLabel lblSubTotal;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JPanel pnlEndDatePicker;
    private javax.swing.JPanel pnlStartDatePicker;
    private javax.swing.JTable tbReceipt;
    private javax.swing.JTable tbReceiptDetail;
    // End of variables declaration//GEN-END:variables

    private void refreshTable() {
        receiptModel.fireTableDataChanged();
        receiptDetailModel.fireTableDataChanged();
    }
}
