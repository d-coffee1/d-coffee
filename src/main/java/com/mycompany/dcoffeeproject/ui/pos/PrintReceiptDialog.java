/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JDialog.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.pos;

import com.mycompany.dcoffeeproject.model.Receipt;
import com.mycompany.dcoffeeproject.model.ReceiptDetail;
import com.mycompany.dcoffeeproject.model.Salary;
import static com.mycompany.dcoffeeproject.ui.product.ProductPanel.tbProduct;
import java.awt.Color;
import java.awt.Frame;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

/**
 *
 * @author ส้มส้ม
 */
public class PrintReceiptDialog extends javax.swing.JDialog {

    private POSPanel posPanel;
    private Receipt receipt;
    private final AbstractTableModel model;

    /**
     * Creates new form PrintReceiptDialog
     */
    public PrintReceiptDialog(POSPanel posPanel, Receipt receipt) {
        super((Frame) null, true);
        initComponents();
        this.posPanel = posPanel;
        this.receipt = receipt;
        JTableHeader header = tbRecieptDetail.getTableHeader();
        header.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        header.setBackground(Color.decode("#7c3893"));
        header.setForeground(Color.decode("#f3edf5"));
        getContentPane().setBackground(Color.decode("#f3edf5"));
        if (receipt.getMember() != null) {
            lblMember.setText(receipt.getMember().getName() + " " + receipt.getMember().getSurname());
        } else {
            lblMember.setText("-");
        }
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
        String receiptDate = dateFormat.format(receipt.getCreatedDate());
        lblDate.setText("วันที่ : " + receiptDate);
        lblEmp.setText("พนักงาน : " + receipt.getEmployee().getName() + " " + receipt.getEmployee().getSurname());
        model = new AbstractTableModel() {
            String[] columnNames = {"รายการ", "ราคาต่อหน่วย", "จำนวน", "ราคารวม"};
            List<ReceiptDetail> receiptDetails = receipt.getReceiptDetails();

            @Override
            public String getColumnName(int column) {
                return columnNames[column];
            }

            @Override
            public int getRowCount() {
                return receiptDetails.size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ReceiptDetail receiptDetail = receiptDetails.get(rowIndex);
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
                String formattedValue = decimalFormat.format(receiptDetail.getTotalPrice());
                switch (columnIndex) {
                    case 0:
                        return receiptDetail.getName();
                    case 1:
                        return receiptDetail.getPrice();
                    case 2:
                        return receiptDetail.getQty();
                    case 3:
                        return formattedValue;
                    default:
                        return "Unknown";
                }
            }
        };
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
        String formattedSubtotal = decimalFormat.format(receipt.getSubtotal());
        String formattedDiscount = decimalFormat.format(receipt.getDiscount());
        String formattedTotal = decimalFormat.format(receipt.getTotal());
        String formattedReceive = decimalFormat.format(receipt.getReceive());
        String formattedChange = decimalFormat.format(receipt.getChange());
        System.out.println(receipt.getChange() +" " + formattedChange);
        tbRecieptDetail.setModel(model);
        lblSubtotal.setText(formattedSubtotal);
        lblDiscount.setText(formattedDiscount);
        lblTotal.setText(formattedTotal);
        lblReceive.setText(formattedReceive);
        lblChange.setText(formattedChange);
        lblPaymentMethod.setText(receipt.getPaymentMethod());

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        lblDate = new javax.swing.JLabel();
        lblEmp = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbRecieptDetail = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        lblSubtotal = new javax.swing.JLabel();
        lblDiscount = new javax.swing.JLabel();
        lblTotal = new javax.swing.JLabel();
        lblReceive = new javax.swing.JLabel();
        lblChange = new javax.swing.JLabel();
        lblMember = new javax.swing.JLabel();
        lblPaymentMethod = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("ใบเสร็จร้าน D-Coffee");

        lblDate.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblDate.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        lblDate.setText("วันที่ :");

        lblEmp.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblEmp.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblEmp.setText("พนักงาน : ");

        tbRecieptDetail.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbRecieptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tbRecieptDetail);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel4.setText("ยอดรวม");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel5.setText("ส่วนลด");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel6.setText("ยอดรวมสุทธิ");

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel7.setText("เงินที่รับมา");

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel8.setText("เงินทอน");

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel9.setText("สมาชิก");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel10.setText("ช่องทางการชำระเงิน");

        jLabel11.setFont(new java.awt.Font("Leelawadee UI", 1, 18)); // NOI18N
        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("---------------------------------------------------------------------------------------------");

        lblSubtotal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblSubtotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblSubtotal.setText("0.0");

        lblDiscount.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblDiscount.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblDiscount.setText("0.0");

        lblTotal.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblTotal.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblTotal.setText("0.0");

        lblReceive.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblReceive.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblReceive.setText("0.0");

        lblChange.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblChange.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblChange.setText("0.0");

        lblMember.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblMember.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblMember.setText("-");

        lblPaymentMethod.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lblPaymentMethod.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        lblPaymentMethod.setText("-");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(184, 184, 184)
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(lblDate, javax.swing.GroupLayout.PREFERRED_SIZE, 178, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblEmp, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, 654, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel8)
                                    .addComponent(jLabel9)
                                    .addComponent(jLabel10))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblSubtotal, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblDiscount, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblTotal, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblReceive, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblChange, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblMember, javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(lblPaymentMethod, javax.swing.GroupLayout.Alignment.TRAILING))))
                        .addGap(15, 15, 15))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(jLabel1)
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblEmp)
                    .addComponent(lblDate))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 283, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(lblSubtotal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(lblDiscount))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(lblTotal))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lblReceive))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(lblChange))
                .addGap(15, 15, 15)
                .addComponent(jLabel11)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(lblMember))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(lblPaymentMethod))
                .addContainerGap(53, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lblChange;
    private javax.swing.JLabel lblDate;
    private javax.swing.JLabel lblDiscount;
    private javax.swing.JLabel lblEmp;
    private javax.swing.JLabel lblMember;
    private javax.swing.JLabel lblPaymentMethod;
    private javax.swing.JLabel lblReceive;
    private javax.swing.JLabel lblSubtotal;
    private javax.swing.JLabel lblTotal;
    private javax.swing.JTable tbRecieptDetail;
    // End of variables declaration//GEN-END:variables
}
