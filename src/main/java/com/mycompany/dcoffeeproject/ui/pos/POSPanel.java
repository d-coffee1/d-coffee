/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.pos;

import com.mycompany.dcoffeeproject.model.Bill;
import com.mycompany.dcoffeeproject.ui.member.AddMemberDialog;
import com.mycompany.dcoffeeproject.model.Member;
import com.mycompany.dcoffeeproject.model.Product;
import com.mycompany.dcoffeeproject.model.Receipt;
import com.mycompany.dcoffeeproject.model.ReceiptDetail;
import com.mycompany.dcoffeeproject.service.EmployeeService;
import com.mycompany.dcoffeeproject.service.MemberService;
import com.mycompany.dcoffeeproject.service.ProductService;
import com.mycompany.dcoffeeproject.service.ReceiptService;
import com.mycompany.dcoffeeproject.ui.productcomponent.BuyProductable;
import com.mycompany.dcoffeeproject.ui.productcomponent.DrinkProductListPanel;
import com.mycompany.dcoffeeproject.ui.productcomponent.BakeryProductListPanel;
import java.awt.Color;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.table.AbstractTableModel;
import javax.swing.table.JTableHeader;

/**
 *
 * @author ส้มส้ม
 */
public class POSPanel extends javax.swing.JPanel implements BuyProductable {

    private ArrayList<Product> products;
    private EmployeeService employeeService = new EmployeeService();
    private ProductService productService = new ProductService();
    private ReceiptService recieptService = new ReceiptService();
    private Receipt receipt;
    private final DrinkProductListPanel drinkProductListPanel;
    private BakeryProductListPanel bakeryProductListPanel;
    private MemberService memberService;
    private List<Member> members;
    private Receipt showReceipt;

    public void setReceipt(Receipt receipt) {
        this.receipt = receipt;
    }

    public Receipt getShowReceipt() {
        return showReceipt;
    }

    public void setShowReceipt(Receipt showReceipt) {
        this.showReceipt = showReceipt;
    }

    public POSPanel() {
        initComponents();
        initProductTable();

        btnUsePoint.setVisible(false);
        btnClearMember.setVisible(false);
        btnClearReceiptDetail.setVisible(false);
        btnFindMember.setBackground(Color.decode("#b465c8"));
        btnCreateMember.setBackground(Color.decode("#b465c8"));
        btnClearMember.setBackground(Color.decode("#b465c8"));
        btnClearReceiptDetail.setBackground(Color.decode("#b465c8"));
        btnPay.setBackground(Color.decode("#d82c98"));
        btnUsePoint.setBackground(Color.decode("#b465c8"));
        setSize(WIDTH, HEIGHT);
        receipt = new Receipt();
        receipt.setEmployee(EmployeeService.currentEmployee);
        JTableHeader header = tbRecieptDetail.getTableHeader();
        header.setFont(new java.awt.Font("Tahoma", java.awt.Font.BOLD, 14));
        tbRecieptDetail.setModel(new AbstractTableModel() {
            String[] headers = {"รายการ", "ราคาต่อหน่วย", "จำนวน", "ราคารวม"};

            @Override
            public String getColumnName(int column) {
                return headers[column];
            }

            @Override
            public int getRowCount() {
                return receipt.getReceiptDetails().size();
            }

            @Override
            public int getColumnCount() {
                return 4;
            }

            @Override
            public Object getValueAt(int rowIndex, int columnIndex) {
                ArrayList<ReceiptDetail> recieptDetails = receipt.getReceiptDetails();
                ReceiptDetail recieptDetail = recieptDetails.get(rowIndex);
                DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
                String formattedValue = decimalFormat.format(recieptDetail.getTotalPrice());
                switch (columnIndex) {
                    case 0:
                        return recieptDetail.getName();
                    case 1:
                        return recieptDetail.getPrice();
                    case 2:
                        return recieptDetail.getQty();
                    case 3:
                        return formattedValue;
                    default:
                        return "";
                }
            }
// บวกค่าrecirptdetail เล็กสุดใหม่

            @Override
            public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
                ArrayList<ReceiptDetail> recieptDetails = receipt.getReceiptDetails();
                ReceiptDetail recieptDetail = recieptDetails.get(rowIndex);
                if (columnIndex == 2) {
                    int qty = Integer.parseInt((String) aValue);
                    if (qty < 1) {
                        return;
                    }
                    recieptDetail.setQty(qty);
                    receipt.calculateSubTotal();
                    refreshReceipt();
                }
            }

            @Override
            public boolean isCellEditable(int rowIndex, int columnIndex) {
                switch (columnIndex) {
                    case 2:
                        return true;
                    default:
                        return false;
                }
            }

        });

        drinkProductListPanel = new DrinkProductListPanel();
        bakeryProductListPanel = new BakeryProductListPanel();
        drinkProductListPanel.addOnByProduct(this);
        bakeryProductListPanel.addOnByProduct(this);
        scrDrinkProductList.setViewportView(drinkProductListPanel);
        scrBakeryProductList.setViewportView(bakeryProductListPanel);
    }

    private void refreshReceipt() {
        DecimalFormat decimalFormat = new DecimalFormat("#,##0.0");
        String formattedValueSubtotal = decimalFormat.format(receipt.getSubtotal());
        String formattedValueDiscount = decimalFormat.format(receipt.getDiscount());
        String formattedValueTotal = decimalFormat.format(receipt.getTotal());
        tbRecieptDetail.revalidate();
        tbRecieptDetail.repaint();
        txtSubtotal.setText(formattedValueSubtotal);
        txtDiscount.setText(formattedValueDiscount);
        txtTotal.setText(formattedValueTotal);
    }

    private void initProductTable() {
        products = productService.getProductsOrderByName();
    }

    private void openAddMemberDialog() {
        AddMemberDialog memberDialog = new AddMemberDialog(new Member());
        //ทำให้เฟรมตามตัวโปรแกรมหบลัก
        memberDialog.setLocationRelativeTo(this);
        memberDialog.setVisible(true);
        //ทำให้เมื่อกดปิดdialog จะรีเฟรชตารางหน้าหลัก
    }

    private void openFindMemberDialog() {
//        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        FindMemberDialog findMemberDialog = new FindMemberDialog(this, receipt);
        findMemberDialog.setLocationRelativeTo(this);
        findMemberDialog.setVisible(true);
        findMemberDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                if (receipt.getMember() != null) {
                    btnUsePoint.setVisible(true);
                    btnClearMember.setVisible(true);
                    lblMemberName.setText(receipt.getMember().getName() + " " + receipt.getMember().getSurname());
                }
            }
        });
    }

    private void openPayDialog() {
//        JFrame frame = (JFrame) SwingUtilities.getRoot(this);
        PayDialog payDialog = new PayDialog(this, receipt);
        payDialog.setLocationRelativeTo(this);
        payDialog.setVisible(true);
        payDialog.addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosed(WindowEvent e) {
                if (receipt.getPaymentMethod().equals("QR Code") || receipt.getPaymentMethod().equals("บัตรเครดิต")) {
                    Receipt saveReceipt = recieptService.addNew(receipt);
                    showReceipt = recieptService.getById(saveReceipt.getId());
                    clearReceipt();
                    openPrintReceiptDialog();
                } else {
                    openCashPaymentDialog();
                }

            }
        });
    }

    public void openPrintReceiptDialog() {
        PrintReceiptDialog printReceiptDialog = new PrintReceiptDialog(this, showReceipt);
        printReceiptDialog.setLocationRelativeTo(this);
        printReceiptDialog.setVisible(true);
    }

    private void openCashPaymentDialog() {
        CashPaymentDialog cashPaymentDialog = new CashPaymentDialog(this, receipt);
        cashPaymentDialog.setLocationRelativeTo(this);
        cashPaymentDialog.setVisible(true);
        cashPaymentDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {

            }
        });
    }

    /**
     * Creates new form POS
     */
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbRecieptDetail = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txtSubtotal = new javax.swing.JLabel();
        txtDiscount = new javax.swing.JLabel();
        txtTotal = new javax.swing.JLabel();
        lblMN = new javax.swing.JLabel();
        lblMemberName = new javax.swing.JLabel();
        btnPay = new javax.swing.JButton();
        btnUsePoint = new javax.swing.JButton();
        btnClearReceiptDetail = new javax.swing.JButton();
        btnClearMember = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        btnFindMember = new javax.swing.JButton();
        btnCreateMember = new javax.swing.JButton();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        scrDrinkProductList = new javax.swing.JScrollPane();
        scrBakeryProductList = new javax.swing.JScrollPane();

        jPanel1.setBackground(new java.awt.Color(245, 237, 245));

        jScrollPane3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        tbRecieptDetail.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbRecieptDetail.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tbRecieptDetail);

        jPanel2.setOpaque(false);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel1.setText("ราคารวม");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel2.setText("ส่วนลด");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel3.setText("ราคารวมสุทธิ");

        txtSubtotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtSubtotal.setText("0.0");

        txtDiscount.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtDiscount.setText("0.0");

        txtTotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        txtTotal.setText("0.0");

        lblMN.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblMN.setText("สมาชิก ");

        lblMemberName.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lblMemberName.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);

        btnPay.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        btnPay.setForeground(new java.awt.Color(243, 237, 245));
        btnPay.setText("ชำระเงิน");
        btnPay.setBorder(null);
        btnPay.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPayActionPerformed(evt);
            }
        });

        btnUsePoint.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnUsePoint.setForeground(new java.awt.Color(243, 237, 245));
        btnUsePoint.setText("ใช้คะแนน");
        btnUsePoint.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnUsePoint.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUsePointActionPerformed(evt);
            }
        });

        btnClearReceiptDetail.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        btnClearReceiptDetail.setForeground(new java.awt.Color(243, 237, 245));
        btnClearReceiptDetail.setText("ยกเลิกรายการ");
        btnClearReceiptDetail.setBorder(null);
        btnClearReceiptDetail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearReceiptDetailActionPerformed(evt);
            }
        });

        btnClearMember.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnClearMember.setForeground(new java.awt.Color(243, 237, 245));
        btnClearMember.setText("ยกเลิก");
        btnClearMember.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnClearMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnClearMemberActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel2)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 314, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtTotal, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtSubtotal, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(txtDiscount, javax.swing.GroupLayout.Alignment.TRAILING))
                        .addGap(10, 10, 10))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lblMN)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnUsePoint, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btnClearMember, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblMemberName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btnClearReceiptDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 172, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnPay, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblMemberName, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lblMN)
                            .addComponent(btnUsePoint)
                            .addComponent(btnClearMember))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txtSubtotal)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtDiscount)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtTotal))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)))
                .addGap(30, 30, 30)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnPay, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnClearReceiptDetail, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel3.setOpaque(false);
        jPanel3.setLayout(new java.awt.GridLayout(1, 0, 10, 10));

        btnFindMember.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnFindMember.setForeground(new java.awt.Color(243, 237, 245));
        btnFindMember.setText("ค้นหาสมาชิก");
        btnFindMember.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnFindMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFindMemberActionPerformed(evt);
            }
        });
        jPanel3.add(btnFindMember);

        btnCreateMember.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btnCreateMember.setForeground(new java.awt.Color(243, 237, 245));
        btnCreateMember.setText("สมัครสมาชิก");
        btnCreateMember.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        btnCreateMember.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCreateMemberActionPerformed(evt);
            }
        });
        jPanel3.add(btnCreateMember);

        jTabbedPane1.setForeground(new java.awt.Color(124, 56, 147));
        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jTabbedPane1.setOpaque(true);

        scrDrinkProductList.setOpaque(false);
        jTabbedPane1.addTab("เครื่องดื่ม", scrDrinkProductList);

        scrBakeryProductList.setBorder(null);
        jTabbedPane1.addTab("เบเกอรี่", scrBakeryProductList);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 585, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 512, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 496, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnCreateMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCreateMemberActionPerformed
        openAddMemberDialog();
    }//GEN-LAST:event_btnCreateMemberActionPerformed

    private void btnFindMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFindMemberActionPerformed
        openFindMemberDialog();
    }//GEN-LAST:event_btnFindMemberActionPerformed

    private void btnPayActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPayActionPerformed
        if (receipt.getReceiptDetails().isEmpty()) {
            JOptionPane.showMessageDialog(this, "กรุณาเลือกรายการสินค้า");
        } else {
            openPayDialog();
            btnClearMember.setVisible(false);
            btnClearReceiptDetail.setVisible(false);
        }
    }//GEN-LAST:event_btnPayActionPerformed

    private void btnUsePointActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnUsePointActionPerformed
        openUsePointDialog();
    }//GEN-LAST:event_btnUsePointActionPerformed

    private void btnClearReceiptDetailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearReceiptDetailActionPerformed
        receipt.delReceiptDetail();
        btnClearReceiptDetail.setVisible(false);
        btnClearMember.setVisible(false);
        clearReceipt();
    }//GEN-LAST:event_btnClearReceiptDetailActionPerformed

    private void btnClearMemberActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnClearMemberActionPerformed
        receipt.clearMember();
        lblMemberName.setText("");
        btnClearMember.setVisible(false);
        btnUsePoint.setVisible(false);
        txtDiscount.setText(String.valueOf(receipt.getDiscount()));
        txtTotal.setText(String.valueOf(receipt.getTotal()));
    }//GEN-LAST:event_btnClearMemberActionPerformed

    private void openUsePointDialog() {
        UsePointDialog usePointDialog = new UsePointDialog(this, receipt);
        usePointDialog.setLocationRelativeTo(this);
        usePointDialog.setVisible(true);
        usePointDialog.addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                refreshReceipt();
            }
        });
    }

    public void clearReceipt() {
        receipt = new Receipt();
        receipt.setEmployee(employeeService.getCurrentEmployee());
        btnUsePoint.setVisible(false);
        lblMemberName.setText("");
        refreshReceipt();
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnClearMember;
    private javax.swing.JButton btnClearReceiptDetail;
    private javax.swing.JButton btnCreateMember;
    private javax.swing.JButton btnFindMember;
    private javax.swing.JButton btnPay;
    private javax.swing.JButton btnUsePoint;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblMN;
    private javax.swing.JLabel lblMemberName;
    private javax.swing.JScrollPane scrBakeryProductList;
    private javax.swing.JScrollPane scrDrinkProductList;
    private javax.swing.JTable tbRecieptDetail;
    private javax.swing.JLabel txtDiscount;
    private javax.swing.JLabel txtSubtotal;
    private javax.swing.JLabel txtTotal;
    // End of variables declaration//GEN-END:variables
    @Override
    public void buy(Product product, String name, String size, String type, String sweet, Double price, int qty) {
        btnClearReceiptDetail.setVisible(true);
        boolean productExists = false;
        // Check if the product already exists in the receipt details
        for (ReceiptDetail detail : receipt.getReceiptDetails()) {
            if (detail.getName().equals(name)) {
                detail.setQty(detail.getQty() + qty);
                receipt.calculateSubTotal();
                productExists = true;
                break;
            }
        }
        // If the product doesn't exist, add a new receipt detail
        if (!productExists) {
            receipt.addReceiptDetail(product, name, size, type, sweet, price, qty);
        }
        refreshReceipt();
    }
}
