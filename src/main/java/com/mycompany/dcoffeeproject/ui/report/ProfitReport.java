/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mildm
 */
public class ProfitReport {

    private String month;
    private Double totalCost;
    private Double totalSales;
    private Double profit;

    public ProfitReport(String month, Double totalCost, Double totalSales, Double profit) {
        this.month = month;
        this.totalCost = totalCost;
        this.totalSales = totalSales;
        this.profit = profit;
    }
      public ProfitReport() {
        this.month = "";
        this.totalCost = 0.0;
        this.totalSales = 0.0;
        this.profit = 0.0;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public Double getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(Double totalCost) {
        this.totalCost = totalCost;
    }

    public Double getTotalSales() {
        return totalSales;
    }

    public void setTotalSales(Double totalSales) {
        this.totalSales = totalSales;
    }

    public Double getProfit() {
        return profit;
    }

    public void setProfit(Double profit) {
        this.profit = profit;
    }

    @Override
    public String toString() {
        return "ProfitReport{" + "month=" + month + ", totalCost=" + totalCost + ", totalSales=" + totalSales + ", profit=" + profit + '}';
    }
    
    public static ProfitReport fromRS(ResultSet rs) {
        ProfitReport pfr = new ProfitReport();
        try {
//            pfr.setMonth(rs.getString("month"));
            pfr.setTotalCost(rs.getDouble("total_expense"));
            pfr.setTotalSales(rs.getDouble("total_income"));
            pfr.setProfit(rs.getDouble("profit"));

        } catch (SQLException ex) {
            Logger.getLogger(ProfitReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return pfr;
    }
}
