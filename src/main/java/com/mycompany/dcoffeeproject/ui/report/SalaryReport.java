/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.report;

import com.mycompany.dcoffeeproject.model.Employee;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author User
 */
public class SalaryReport {
    private int employeeId;
    private Employee employee;
    private String employeeName, employeeSurname;
    private Double employeeHourWage;
    private int totalWorkHours;
    private Double totalSalary;
    
    public SalaryReport(int employeeId, String employeeName, String employeeSurname, Double employeeHourWage, int totalWorkHours, Double totalSalary) {
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.employeeSurname = employeeSurname;
        this.employeeHourWage = employeeHourWage;
        this.totalWorkHours = totalWorkHours;
        this.totalSalary = totalSalary;
    }
    
    public SalaryReport(String employeeName, String employeeSurname, Double employeeHourWage, int totalWorkHours, Double totalSalary) {
        this.employeeId = -1;
        this.employeeName = "";
        this.employeeSurname = "";
        this.employeeHourWage = 0.0;
        this.totalWorkHours = 0;
        this.totalSalary = 0.0;
    }
    
    public SalaryReport() {
        this.employeeId = -1;
        this.employeeName = "";
        this.employeeSurname = "";
        this.employeeHourWage = 0.0;
        this.totalWorkHours = 0;
        this.totalSalary = 0.0;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeSurname() {
        return employeeSurname;
    }

    public void setEmployeeSurname(String employeeSurname) {
        this.employeeSurname = employeeSurname;
    }

    public Double getEmployeeHourWage() {
        return employeeHourWage;
    }

    public void setEmployeeHourWage(Double employeeHourWage) {
        this.employeeHourWage = employeeHourWage;
    }

    public int getTotalWorkHours() {
        return totalWorkHours;
    }

    public void setTotalWorkHours(int totalWorkHours) {
        this.totalWorkHours = totalWorkHours;
    }

    public Double getTotalSalary() {
        return totalSalary;
    }

    public void setTotalSalary(Double totalSalary) {
        this.totalSalary = totalSalary;
    }

    @Override
    public String toString() {
        return "SalaryReport{" + "employeeId=" + employeeId + ", employee=" + employee + ", employeeName=" + employeeName + ", employeeSurname=" + employeeSurname + ", employeeHourWage=" + employeeHourWage + ", totalWorkHours=" + totalWorkHours + ", totalSalary=" + totalSalary + '}';
    }
    
     public static SalaryReport fromRS(ResultSet rs) {
        SalaryReport slr = new SalaryReport();
        try {
            slr.setEmployeeId(rs.getInt("employee_id"));
            slr.setEmployeeName(rs.getString("employee_name"));
            slr.setEmployeeSurname(rs.getString("employee_surname"));
            slr.setEmployeeHourWage(rs.getDouble("employee_hour_wage"));
            slr.setTotalWorkHours(rs.getInt("total_work_hours"));
            slr.setTotalSalary(rs.getDouble("total_salary"));

        } catch (SQLException ex) {
            Logger.getLogger(SalaryReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return slr;
     }
}
