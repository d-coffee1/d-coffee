/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mildm
 */
public class ProductReport {

    private int id;
    private String name;
    private Double totalPrice;
    private int totalQty;

    public ProductReport(int id, String name, Double totalPrice, int totalQty) {
        this.id = id;
        this.name = name;
        this.totalPrice = totalPrice;
        this.totalQty = totalQty;
    }

    public ProductReport(String name, Double totalPrice, int totalQty) {
        this.id = -1;
        this.name = name;
        this.totalPrice = totalPrice;
        this.totalQty = totalQty;
    }
    public ProductReport() {
        this.id = -1;
        this.name = "";
        this.totalPrice = 0.0;
        this.totalQty =  0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    @Override
    public String toString() {
        return "ProductReport{" + "id=" + id + ", name=" + name + ", totalPrice=" + totalPrice + ", totalQty=" + totalQty + '}';
    }

    public static ProductReport fromRS(ResultSet rs) {
        ProductReport pdr = new ProductReport();
        try {
            pdr.setId(rs.getInt("Product_id"));
            pdr.setName(rs.getString("Product_name"));
            pdr.setTotalPrice(rs.getDouble("Total_Price"));
            pdr.setTotalQty(rs.getInt("Total_Qty"));

        } catch (SQLException ex) {
            Logger.getLogger(ProductReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return pdr;
    }
}
