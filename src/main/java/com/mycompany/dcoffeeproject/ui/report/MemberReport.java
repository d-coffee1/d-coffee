/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.ui.report;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mildm
 */
public class MemberReport {

    private int id;
    private String name;
    private String surname;
    private String phone;
    private Double totalSpent;

    public MemberReport(int id, String name, String surname, Double totalSpent) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.totalSpent = totalSpent;
    }

    public MemberReport(String name, String surname, Double totalSpent) {
        this.id = -1;
        this.name = name;
        this.surname = surname;
        this.totalSpent = totalSpent;
    }
    
    public MemberReport() {
        this.id = -1;
        this.name = "";
        this.surname = "";
        this.totalSpent = 0.0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Double getTotalSpent() {
        return totalSpent;
    }

    public void setTotalSpent(Double totalSpent) {
        this.totalSpent = totalSpent;
    }

    @Override
    public String toString() {
        return "MemberReport{" + "id=" + id + ", name=" + name + ", surname=" + surname + ", totalSpent=" + totalSpent + '}';
    }

    public static MemberReport fromRS(ResultSet rs) {
        MemberReport mbr = new MemberReport();
        try {
            mbr.setId(rs.getInt("member_id"));
            mbr.setName(rs.getString("member_name"));
            mbr.setSurname(rs.getString("member_surname"));
            mbr.setPhone(rs.getString("member_phone"));
            mbr.setTotalSpent(rs.getDouble("total_spending"));
            
        } catch (SQLException ex) {
            Logger.getLogger(MemberReport.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return mbr;
    }
}
