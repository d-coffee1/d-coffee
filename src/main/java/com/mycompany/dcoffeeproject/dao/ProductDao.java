/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.Product;
import com.mycompany.dcoffeeproject.ui.report.ProductReport;
import com.mycompany.dcoffeeproject.ui.report.ProductWorseReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author thanc
 */
public class ProductDao implements Dao<Product>{

    @Override
    public Product getOne(int id) {
       Product product = null;
        String sql = "SELECT * FROM Product WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                product = Product.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return product;
    }

    @Override
    public List<Product> getAll() {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM Product";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
            
            while(rs.next()) {
                Product product = Product.fromRS(rs);               
                list.add(product);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Product> getAll(String order) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
        public List<Product> getAllByCategory(int id) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM product WHERE category_id=" + id;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Product product = Product.fromRS(rs);
                list.add(product);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Product save(Product obj) {
        Product product = null;
        String sql = "INSERT INTO Product (name, price, category_id,available_type)"
                +"VALUES(?, ?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setDouble(2, obj.getPrice());
            stmt.setInt(3, obj.getCategoryId());
            stmt.setString(4, obj.getAvailableType());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
                       
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Product update(Product obj) {
        String sql = "UPDATE Product"
                + " SET name = ?, price = ?, category_id = ?, available_type=?"
                + " WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setDouble(2, obj.getPrice());
            stmt.setInt(3, obj.getCategoryId());
            stmt.setString(4, obj.getAvailableType());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Product obj) {
        String sql = "DELETE FROM Product WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;   
    }

    @Override
    public List<Product> getAllByName(String name) {
        ArrayList<Product> list = new ArrayList();
        String sql = "SELECT * FROM Product WHERE name LIKE '%"+name+"%'";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);
    
            while(rs.next()) {
                Product product = Product.fromRS(rs);
                System.out.println(product.toString());
                list.add(product);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ProductReport> getProductTopten(int limit) {
        ArrayList<ProductReport> list = new ArrayList();
        String sql = """
                     SELECT Product.id AS Product_id,Product.name AS Product_name,SUM(ReceiptItem.qty) AS Total_Qty,SUM(ReceiptItem.total) AS Total_Price 
                     FROM Receipt INNER JOIN ReceiptItem ON Receipt.id = ReceiptItem.receipt_id INNER JOIN Product ON ReceiptItem.product_id = Product.id  
                     GROUP BY Product.id ORDER BY Total_Price DESC
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProductReport obj = ProductReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ProductReport> getProductTopten(String begin, String end, int limit) {
        ArrayList<ProductReport> list = new ArrayList();
        String sql = """
                     SELECT Product.id AS Product_id,Product.name AS Product_name,SUM(ReceiptItem.qty) AS  Total_Qty,SUM(ReceiptItem.total) AS Total_Price  
                     FROM Receipt INNER JOIN ReceiptItem ON Receipt.id = ReceiptItem.receipt_id INNER JOIN Product ON ReceiptItem.product_id = Product.id  
                     WHERE DATE(Receipt.date) BETWEEN ? AND ? GROUP BY Product.id ORDER BY Total_Price DESC
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProductReport obj = ProductReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ProductWorseReport> getProductToptenWorse(int limit) {
        ArrayList<ProductWorseReport> list = new ArrayList();
        String sql = """
                     SELECT Product.id AS Product_id,Product.name AS Product_name,SUM(ReceiptItem.qty) AS Total_Qty,SUM(ReceiptItem.total) AS Total_Price 
                     FROM Receipt INNER JOIN ReceiptItem ON Receipt.id = ReceiptItem.receipt_id INNER JOIN Product ON ReceiptItem.product_id = Product.id  
                     GROUP BY Product.id ORDER BY Total_Price ASC
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProductWorseReport obj = ProductWorseReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ProductWorseReport> getProductToptenWorse(String begin, String end, int limit) {
        ArrayList<ProductWorseReport> list = new ArrayList();
        String sql = """
                     SELECT Product.id AS Product_id,Product.name AS Product_name,SUM(ReceiptItem.qty) AS  Total_Qty,SUM(ReceiptItem.total) AS Total_Price  
                     FROM Receipt INNER JOIN ReceiptItem ON Receipt.id = ReceiptItem.receipt_id INNER JOIN Product ON ReceiptItem.product_id = Product.id  
                     WHERE DATE(Receipt.date) BETWEEN ? AND ? GROUP BY Product.id ORDER BY Total_Price ASC
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProductWorseReport obj = ProductWorseReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    
}
