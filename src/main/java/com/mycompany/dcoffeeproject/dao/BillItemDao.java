/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.BillItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class BillItemDao implements Dao<BillItem> {

    public BillItem get(int id) {
        BillItem billItem = null;
        String sql = "SELECT * FROM BillItem WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                billItem = BillItem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return billItem;
    }

    public List<BillItem> getByBillId(int id) {
        ArrayList<BillItem> list = new ArrayList();
        String sql = "SELECT * FROM BillItem WHERE bill_id=" + id + "";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillItem billItem = BillItem.fromRS(rs);
                list.add(billItem);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<BillItem> getAll() {
        ArrayList<BillItem> list = new ArrayList();
        String sql = "SELECT * FROM BillItem";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillItem billItem = BillItem.fromRS(rs);
                list.add(billItem);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<BillItem> getAll(String where, String order) {
        ArrayList<BillItem> list = new ArrayList();
        String sql = "SELECT * FROM BillItem WHERE " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillItem billItem = BillItem.fromRS(rs);
                list.add(billItem);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<BillItem> getAll(String order) {
        ArrayList<BillItem> list = new ArrayList();
        String sql = "SELECT * FROM BillItem  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                BillItem billItem = BillItem.fromRS(rs);
                list.add(billItem);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public BillItem save(BillItem obj) {

        String sql = "INSERT INTO BillItem (material_id,material_name,price,qty,total,bill_id)"
                + "VALUES(?, ? ,? ,? ,? ,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMaterialId());
            stmt.setString(2, obj.getMaterialName());
            stmt.setDouble(3, obj.getPrice());
            stmt.setInt(4, obj.getQty());
            stmt.setDouble(5, obj.getTotal());
            stmt.setInt(6, obj.getBillId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public BillItem update(BillItem obj) {
        String sql = "UPDATE BillItem"
                + " SET material_id = ?, material_name = ?, price = ?, qty = ?, total = ?, bill_id = ?"
                + " WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMaterialId());
            stmt.setString(2, obj.getMaterialName());
            stmt.setDouble(3, obj.getPrice());
            stmt.setInt(4, obj.getQty());
            stmt.setDouble(5, obj.getTotal());
            stmt.setInt(6, obj.getBillId());
            stmt.setInt(7, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(BillItem obj) {
        String sql = "DELETE FROM BillItem WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public BillItem getOne(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<BillItem> getAllByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
