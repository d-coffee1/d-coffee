/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.Material;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author thanc
 */
public class MaterialDao implements Dao<Material> {

    @Override
    public Material getOne(int id) {
        Material material = null;
        String sql = "SELECT * FROM Material WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                material = Material.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return material;
    }

    public List<Material> getAllMaterialsASC() {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM Material ORDER BY balance ASC;";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material material = Material.fromRS(rs);
                list.add(material);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public List<Material> getAll() {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM Material";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material material = Material.fromRS(rs);
                list.add(material);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Material> getAllLowBalance() {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM Material WHERE balance < min_balance ORDER BY balance ASC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material material = Material.fromRS(rs);
                list.add(material);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Material save(Material obj) {
        Material material = null;
        String sql = "INSERT INTO Material (name, unit, balance, min_balance,status)"
                + "VALUES(?, ?, ?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getUnit());
            stmt.setInt(3, obj.getBalance());
            stmt.setInt(4, obj.getMinBalance());
            stmt.setString(5, obj.getStatus());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Material update(Material obj) {
        String sql = "UPDATE Material"
                + " SET name = ?, unit = ?, balance = ?, min_balance = ?, status = ?"
                + " WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getUnit());
            stmt.setInt(3, obj.getBalance());
            stmt.setInt(4, obj.getMinBalance());
            stmt.setString(5, obj.getStatus());
            stmt.setInt(6, obj.getId());
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Material obj) {
        String sql = "DELETE FROM Material WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Material> getAllByName(String name) {
        ArrayList<Material> list = new ArrayList();
        String sql = "SELECT * FROM Material WHERE name LIKE '%" + name + "%'";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Material material = Material.fromRS(rs);
                list.add(material);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }

}
