/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.ReceiptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ReceiptDetailDao implements Dao<ReceiptDetail> {

    public ReceiptDetail get(int id) {
        ReceiptDetail recieptDetail = null;
        String sql = "SELECT * FROM ReceiptItem WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                recieptDetail = ReceiptDetail.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return recieptDetail;
    }

    public List<ReceiptDetail> getAll() {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM ReceiptItem";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail recieptDetail = ReceiptDetail.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<ReceiptDetail> getAllByReceiptId(int receiptId) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM ReceiptItem WHERE receipt_id="+receiptId+"";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail recieptDetail = ReceiptDetail.fromRS(rs);
                list.add(recieptDetail);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReceiptDetail> getAll(String where, String order) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM ReceiptItem where " + where + " ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail recieptDetail = ReceiptDetail.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ReceiptDetail> getAll(String order) {
        ArrayList<ReceiptDetail> list = new ArrayList();
        String sql = "SELECT * FROM ReceiptItem  ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                ReceiptDetail recieptDetail = ReceiptDetail.fromRS(rs);
                list.add(recieptDetail);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public ReceiptDetail save(ReceiptDetail obj) {

        String sql = "INSERT INTO ReceiptItem (product_id, name,size,type,sweet,price, qty, total, receipt_id)"
                + "VALUES(?, ? ,? ,? ,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProductId());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getSize());
            stmt.setString(4, obj.getType());
            stmt.setString(5, obj.getSweet());
            stmt.setDouble(6, obj.getPrice());
            stmt.setDouble(7, obj.getQty());
            stmt.setDouble(8, obj.getTotalPrice());
            stmt.setInt(9, obj.getReceiptId());
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public ReceiptDetail update(ReceiptDetail obj) {
        String sql = "UPDATE ReceiptItem"
                + " SET product_id = ?, name = ?,size = ?,type = ?,sweet =?,price = ?, qty = ?, total = ?, receipt_id = ?"
                + " WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getProductId());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getSize());
            stmt.setString(4, obj.getType());
            stmt.setString(5, obj.getSweet());
            stmt.setDouble(6, obj.getPrice());
            stmt.setDouble(7, obj.getQty());
            stmt.setDouble(8, obj.getTotalPrice());
            stmt.setInt(9, obj.getReceiptId());
            stmt.setInt(10, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(ReceiptDetail obj) {
        String sql = "DELETE FROM ReceiptItem WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public ReceiptDetail getOne(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<ReceiptDetail> getAllByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
