/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.Promotion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thanc
 */
public class PromotionDao implements Dao<Promotion> {

    @Override
    public Promotion getOne(int id) {
        Promotion promotion = null;
        String sql = "SELECT * FROM Promotion WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                promotion = Promotion.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return promotion;
    }

    @Override
    public List<Promotion> getAll() {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM Promotion";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Promotion> getAll(String order) {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM Promotion ORDER BY" + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                list.add(promotion);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Promotion save(Promotion obj) {
        Promotion promotion = null;
        String sql = "INSERT INTO Promotion (name, description, start_date,end_date)"
                + "VALUES(?, ?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            // Parse the input date in the Buddhist calendar format
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date begin = inputFormat.parse(obj.getStartDateStr());
            Date end = inputFormat.parse(obj.getEndDateStr());

            // Create a Calendar instance and set it to the parsed date
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(begin);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(end);
            // Subtract 543 years from the year to convert to the Gregorian calendar
            int gregorianYear1 = calendar1.get(Calendar.YEAR) - 543;
            int gregorianYear2 = calendar2.get(Calendar.YEAR) - 543;

            // Set the year back to the calendar
            calendar1.set(Calendar.YEAR, gregorianYear1);
            calendar2.set(Calendar.YEAR, gregorianYear2);

            // Create a SimpleDateFormat to format the date in the desired format
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            // Format the date as a string in the desired format
            String formattedDate1 = outputFormat.format(calendar1.getTime());
            String formattedDate2 = outputFormat.format(calendar2.getTime());
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getDiscription());
            stmt.setString(3, formattedDate1);
            stmt.setString(4, formattedDate2);
//            System.out.println(stmt);
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(PromotionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    @Override
    public Promotion update(Promotion obj) {
        String sql = "UPDATE Promotion"
                + " SET name = ?, description = ?, start_date = ?, end_date=?"
                + " WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date begin = inputFormat.parse(obj.getStartDateStr());
            Date end = inputFormat.parse(obj.getEndDateStr());

            // Create a Calendar instance and set it to the parsed date
            Calendar calendar1 = Calendar.getInstance();
            calendar1.setTime(begin);
            Calendar calendar2 = Calendar.getInstance();
            calendar2.setTime(end);
            // Subtract 543 years from the year to convert to the Gregorian calendar
            int gregorianYear1 = calendar1.get(Calendar.YEAR) - 543;
            int gregorianYear2 = calendar2.get(Calendar.YEAR) - 543;

            // Set the year back to the calendar
            calendar1.set(Calendar.YEAR, gregorianYear1);
            calendar2.set(Calendar.YEAR, gregorianYear2);

            // Create a SimpleDateFormat to format the date in the desired format
            SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            // Format the date as a string in the desired format
            String formattedDate1 = outputFormat.format(calendar1.getTime());
            String formattedDate2 = outputFormat.format(calendar2.getTime());
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getDiscription());
            stmt.setString(3, formattedDate1);
            stmt.setString(4, formattedDate2);
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
//            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        } catch (ParseException ex) {
            Logger.getLogger(PromotionDao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return obj;
    }

    @Override
    public int delete(Promotion obj) {
        String sql = "DELETE FROM Promotion WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Promotion> getAllByName(String name) {
        ArrayList<Promotion> list = new ArrayList();
        String sql = "SELECT * FROM Promotion WHERE name LIKE '%" + name + "%'";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Promotion promotion = Promotion.fromRS(rs);
                System.out.println(promotion.toString());
                list.add(promotion);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
