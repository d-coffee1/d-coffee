/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.Expense;
import com.mycompany.dcoffeeproject.model.ExpenseItem;
import com.mycompany.dcoffeeproject.ui.report.MemberReport;
import com.mycompany.dcoffeeproject.ui.report.ProfitReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class ExpenseDao implements Dao<Expense> {

    @Override
    public Expense getOne(int id) {
        Expense expense = null;
        String sql = "SELECT * FROM Expense WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                expense = Expense.fromRS(rs);
                ExpenseItemDao rdd = new ExpenseItemDao();
                ArrayList<ExpenseItem> expenseItems = (ArrayList<ExpenseItem>) rdd.getAll(" expense_id =" + expense.getId(), " id ");
                expense.setExpenseItems(expenseItems);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return expense;
    }

    @Override
    public List<Expense> getAll() {
        ArrayList<Expense> list = new ArrayList();
        String sql = "SELECT * FROM Expense ORDER BY id DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Expense expense = Expense.fromRS(rs);
                list.add(expense);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<Expense> getAll(String startDate, String endDate) {
        ArrayList<Expense> list = new ArrayList();
        String sql = "SELECT * FROM Expense WHERE DATE(date) BETWEEN " + "'" + startDate + "'" + " AND " + "'" + endDate + "'" + "ORDER BY id DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Expense expense = Expense.fromRS(rs);
                list.add(expense);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Expense save(Expense obj) {
        String sql = "INSERT INTO Expense (total, employee_id) VALUES (?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotal());
            stmt.setInt(2, obj.getEmployeeId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public int delete(Expense obj) {
        String sql = "DELETE FROM Expense WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Expense> getAllByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Expense update(Expense obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<ProfitReport> getProfitAll(String begin, String end) {
        ArrayList<ProfitReport> list = new ArrayList();
        String sql = """
                     WITH ExpenseTotal AS (
                         SELECT COALESCE(SUM(total), 0) AS total_expense
                         FROM Expense
                         WHERE DATE(date) BETWEEN ? AND ?
                     ),
                     IncomeTotal AS (
                         SELECT COALESCE(SUM(total), 0) AS total_income
                         FROM Receipt
                         WHERE DATE(date) BETWEEN ? AND ?
                     )
                     SELECT total_expense, total_income, total_income - total_expense AS profit
                     FROM ExpenseTotal, IncomeTotal;      
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setString(3, begin);
            stmt.setString(4, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProfitReport obj = ProfitReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ProfitReport> getProfitAll() {
        ArrayList<ProfitReport> list = new ArrayList();
        String sql = """
                     WITH ExpenseTotal AS (
                         SELECT COALESCE(SUM(total), 0) AS total_expense
                         FROM Expense
                     ),
                     IncomeTotal AS (
                         SELECT COALESCE(SUM(total), 0) AS total_income
                         FROM Receipt
                     )
                     SELECT total_expense, total_income, total_income - total_expense AS profit
                     FROM ExpenseTotal, IncomeTotal;      
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ProfitReport obj = ProfitReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
