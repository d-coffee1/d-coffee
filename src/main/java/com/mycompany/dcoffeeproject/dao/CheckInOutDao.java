/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.CheckInOut;
import java.sql.Connection;
import java.util.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 *
 * @author thanc
 */
public class CheckInOutDao implements Dao<CheckInOut> {

    @Override
    public CheckInOut getOne(int id) {
        CheckInOut checkInOut = null;
        String sql = "SELECT * FROM CheckInOut WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();
            while (rs.next()) {
                checkInOut = CheckInOut.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkInOut;
    }

    @Override
    public List<CheckInOut> getAll() {
        ArrayList<CheckInOut> list = new ArrayList<>();
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    Date currentDate = new Date();

    // Convert the current date to Thai calendar (Buddhist Era)
    Calendar cal = Calendar.getInstance();
    cal.setTime(currentDate);
    cal.add(Calendar.YEAR, -543);  // Subtract 543 years to convert to Buddhist Era
    String formattedDate = dateFormat.format(cal.getTime());
    System.out.println(formattedDate.substring(0, 10));
        String sql = "SELECT * FROM CheckInOut WHERE DATE(date) = '" + formattedDate.substring(0, 10) + "' ORDER BY date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<CheckInOut> getAllHistory() {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM CheckInOut WHERE check_out_time NOT null ORDER BY date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
     public List<CheckInOut> getAllHistory(String startDate ,String endDate, int empId) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM CheckInOut WHERE check_out_time NOT null AND employee_id="+empId+" AND DATE(date) BETWEEN "+"'"+startDate+"'"+" AND "+"'"+endDate+"'"+"ORDER BY date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
     
     public List<CheckInOut> getAllHistory(int empId) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM CheckInOut WHERE check_out_time NOT null AND employee_id="+empId+" ORDER BY date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
     
     public List<CheckInOut> getAllHistory(String startDate ,String endDate) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM CheckInOut WHERE check_out_time NOT null AND DATE(date) BETWEEN "+"'"+startDate+"'"+" AND "+"'"+endDate+"'"+"ORDER BY date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<CheckInOut> getAllByEmpId(int id) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM CheckInOut WHERE check_out_time NOT null AND salary_id IS null AND employee_id="+id+" ORDER BY date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                list.add(checkInOut);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckInOut save(CheckInOut obj) {
        CheckInOut checkInOut = null;
        String sql = "INSERT INTO CheckInOut (employee_id)"
                + "VALUES(?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return checkInOut;
    }

    @Override
    public CheckInOut update(CheckInOut obj) {
        String sql = "UPDATE CheckInOut"
                + " SET salary_id = ?"
                + " WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getSalaryId());
            stmt.setInt(2, obj.getId());
            int ret = stmt.executeUpdate();
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public CheckInOut checkOut(CheckInOut obj) {
    String updateCheckOutTimeSql = "UPDATE CheckInOut"
            + " SET check_out_time=?"
            + " WHERE id = ?";
    String updateWorkHoursSql = "UPDATE CheckInOut"
            + " SET work_hours=?"
            + " WHERE id= ?";
    Connection conn = DatabaseHelper.getConnect();
    try {
        PreparedStatement stmt = conn.prepareStatement(updateCheckOutTimeSql);
        // Format checkOutDatetime
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(obj.getCheckOutTime());

        // Adjust the year to Buddhist Era (subtract 543)
        int buddhistYear = calendar.get(Calendar.YEAR) - 543;
        calendar.set(Calendar.YEAR, buddhistYear);

        Timestamp checkOutTimestamp = new Timestamp(calendar.getTime().getTime());
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String checkOutDatetimeString = dateFormat.format(checkOutTimestamp);
        stmt.setString(1, checkOutDatetimeString);
        stmt.setInt(2, obj.getId());
        stmt.executeUpdate();
        CheckInOut updatedCheckInOut = getOne(obj.getId());

        long timeDifference = updatedCheckInOut.getCheckOutTime().getTime() - updatedCheckInOut.getCheckInTime().getTime();

        int totalWorkHours = (int) (timeDifference / 3600000);

        PreparedStatement stmt2 = conn.prepareStatement(updateWorkHoursSql);
        stmt2.setInt(1, totalWorkHours);
        stmt2.setInt(2, updatedCheckInOut.getId());
        stmt2.executeUpdate();
        return obj;
    } catch (SQLException ex) {
        System.out.println(ex.getMessage());
        return null;
    }
}

    @Override
    public int delete(CheckInOut obj) {
        String sql = "DELETE FROM CheckInOut WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    public List<CheckInOut> getAllByEmployeeName(String name) {
        ArrayList<CheckInOut> list = new ArrayList();
        String sql = "SELECT * FROM CheckInOut WHERE name LIKE '%" + name + "%'";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckInOut checkInOut = CheckInOut.fromRS(rs);
                System.out.println(checkInOut.toString());
                list.add(checkInOut);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

//    public List<CheckInOut> getAllByDate(String date) {
//        ArrayList<CheckInOut> list = new ArrayList();
//        String sql = "SELECT * FROM CheckInOut WHERE date=?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            
//            stmt.setDate(1, );
//            while (rs.next()) {
//                CheckInOut checkInOut = CheckInOut.fromRS(rs);
//                System.out.println(checkInOut.toString());
//                list.add(checkInOut);
//            }
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//        }
//        return list;
//    }
    @Override
    public List<CheckInOut> getAllByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
