/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.Salary;
import com.mycompany.dcoffeeproject.ui.report.ProductReport;
import com.mycompany.dcoffeeproject.ui.report.SalaryReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author thanc
 */
public class SalaryDao implements Dao<Salary> {

    @Override
    public Salary getOne(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Salary> getAll() {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM Salary ORDER BY id DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Salary> getAll(int empId) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM Salary WHERE employee_id="+empId+" ORDER BY date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Salary> getAll(String startDate,String endDate) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM Salary WHERE DATE(date) BETWEEN "+"'"+startDate+"'"+" AND "+"'"+endDate+"'"+"ORDER BY date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Salary> getAll(String startDate,String endDate,int empId) {
        ArrayList<Salary> list = new ArrayList();
        String sql = "SELECT * FROM Salary WHERE employee_id="+empId+" AND DATE(date) BETWEEN "+"'"+startDate+"'"+" AND "+"'"+endDate+"'"+"ORDER BY date DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Salary salary = Salary.fromRS(rs);
                list.add(salary);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


@Override
public Salary save(Salary obj) {
        String sql = "INSERT INTO Salary (employee_id,employee_name,employee_surname,employee_hour_wage,total_work_hours,total_salary)"
                + "VALUES(?,?,?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getEmployeeId());
            stmt.setString(2, obj.getEmployeeName());
            stmt.setString(3, obj.getEmployeeSurname());
            stmt.setDouble(4, obj.getEmployeeHourWage());
            stmt.setInt(5, obj.getTotalWorkHours());
            stmt.setDouble(6, obj.getTotalSalary());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
public Salary update(Salary obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
public int delete(Salary obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
public List<Salary> getAllByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

 public List<SalaryReport> getSalary() {
        ArrayList<SalaryReport> list = new ArrayList();
        String sql = """
                     SELECT employee_id, employee_name, employee_surname, employee_hour_wage, SUM(total_work_hours) AS total_work_hours, SUM(total_salary) AS total_salary
                     FROM Salary
                     GROUP BY employee_id;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                SalaryReport obj = SalaryReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<SalaryReport> getSalary(String begin, String end) {
        ArrayList<SalaryReport> list = new ArrayList();
        String sql = """
                     SELECT employee_id, employee_name, employee_surname, employee_hour_wage, SUM(total_work_hours) AS total_work_hours, SUM(total_salary) AS total_salary
                     FROM Salary WHERE DATE(date) BETWEEN ? AND ?
                     GROUP BY employee_id;
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                SalaryReport obj = SalaryReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

}
