/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.Material;
import com.mycompany.dcoffeeproject.model.Member;
import com.mycompany.dcoffeeproject.ui.report.MemberReport;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author thanc
 */
public class MemberDao implements Dao<Member> {
     @Override
    public Member getOne(int id) {
        Member member = null;
        String sql = "SELECT * FROM Member WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                member = Member.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return member;
    }
    
    public Member getOneByPhone(String phone) {
        Member member = null;
        String sql = "SELECT * FROM Member WHERE phone=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, phone);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                member = Member.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return member;
    }

    @Override
    public List<Member> getAll() {
        ArrayList<Member> list = new ArrayList();
        String sql = "SELECT * FROM Member";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Member member = Member.fromRS(rs);
                list.add(member);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Member save(Member obj) {
        Member member = null;
        String sql = "INSERT INTO Member (name, surname, phone)"
                + "VALUES(?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getSurname());
            stmt.setString(3, obj.getPhone());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public Member update(Member obj) {
        String sql = "UPDATE Member"
                + " SET name = ?, surname = ?, phone = ?, point = ?"
                + " WHERE id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getName());
            stmt.setString(2, obj.getSurname());
            stmt.setString(3, obj.getPhone());
            stmt.setInt(4, obj.getPoint());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int ret = stmt.executeUpdate();
            System.out.println(ret);
            return obj;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    @Override
    public int delete(Member obj) {
        String sql = "DELETE FROM Member WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Member> getAllByName(String name) {
        ArrayList<Member> list = new ArrayList();
        String sql = "SELECT * FROM Member WHERE name LIKE '%" + name + "%'";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Member member = Member.fromRS(rs);
                list.add(member);
            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;

    }
    
    public List<MemberReport> getTopSpender(int limit) {
        ArrayList<MemberReport> list = new ArrayList();
        String sql = """
                      SELECT Member.id AS member_id,Member.name AS member_name,Member.surname AS member_surname,Member.phone AS member_phone ,SUM(Receipt.total) AS total_spending 
                      FROM Member INNER JOIN Receipt ON Member.id = Receipt.member_id GROUP BY Member.id ORDER BY total_spending DESC LIMIT ?
                                 
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                MemberReport obj = MemberReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
   public List<MemberReport> getTopSpender(String begin, String end, int limit) {
        ArrayList<MemberReport> list = new ArrayList();
        String sql = """
                     SELECT Member.id AS member_id,Member.name AS member_name,Member.surname AS member_surname,Member.phone AS member_phone ,SUM(Receipt.total) AS total_spending 
                     FROM Member INNER JOIN Receipt ON Member.id = Receipt.member_id WHERE DATE(Receipt.date) BETWEEN ? AND ? 
                     GROUP BY Member.id ORDER BY total_spending DESC LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, begin);
            stmt.setString(2, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                MemberReport obj = MemberReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }


}
