/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.CheckMaterialItem;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author thanc
 */
public class CheckMaterialItemDao implements Dao<CheckMaterialItem>{

    @Override
    public CheckMaterialItem getOne(int id) {
        CheckMaterialItem checkMaterialItem = null;
        String sql = "SELECT * FROM CheckMaterialItem WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                checkMaterialItem = CheckMaterialItem.fromRS(rs);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return checkMaterialItem;
    }

    @Override
    public List<CheckMaterialItem> getAll() {
        ArrayList<CheckMaterialItem> list = new ArrayList();
        String sql = "SELECT * FROM CheckMaterialItem";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterialItem checkMaterialItem = CheckMaterialItem.fromRS(rs);
                list.add(checkMaterialItem);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<CheckMaterialItem> getByCheckMatId(int id) {
        ArrayList<CheckMaterialItem> list = new ArrayList();
        String sql = "SELECT * FROM CheckMaterialItem WHERE check_material_id="+id+"";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                CheckMaterialItem checkMaterialItem = CheckMaterialItem.fromRS(rs);
                list.add(checkMaterialItem);
            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public CheckMaterialItem save(CheckMaterialItem obj) {
        String sql = "INSERT INTO CheckMaterialItem (material_id,last_balance,current_balance,check_material_id)"
                +"VALUES(?, ?, ?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getMaterialId());
            stmt.setInt(2, obj.getLastBalance());
            stmt.setInt(3, obj.getCurrentBalance());
            stmt.setInt(4, obj.getCheckMaterialId());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
                       
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

    @Override
    public CheckMaterialItem update(CheckMaterialItem obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(CheckMaterialItem obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<CheckMaterialItem> getAllByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}
