/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.dao;

import com.mycompany.dcoffeeproject.helper.DatabaseHelper;
import com.mycompany.dcoffeeproject.model.Receipt;
import com.mycompany.dcoffeeproject.model.ReceiptDetail;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ReceiptDao implements Dao<Receipt> {

    @Override
    public Receipt getOne(int id) {
        Receipt reciept = null;
        String sql = "SELECT * FROM Receipt WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                reciept = Receipt.fromRS(rs);
//                ReceiptDetailDao rdd = new ReceiptDetailDao();
////                ArrayList<ReceiptDetail> recieptDetails = (ArrayList<ReceiptDetail>) rdd.getAllByReceiptId(r)
////                reciept.setReceiptDetails(recieptDetails);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return reciept;
    }

    @Override
    public List<Receipt> getAll() {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM Receipt ORDER BY id DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt reciept = Receipt.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
    
    public List<Receipt> getAll(String startDate,String endDate) {
        ArrayList<Receipt> list = new ArrayList();
        String sql = "SELECT * FROM Receipt WHERE DATE(date) BETWEEN "+"'"+startDate+"'"+" AND "+"'"+endDate+"'"+"ORDER BY id DESC";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                Receipt reciept = Receipt.fromRS(rs);
                list.add(reciept);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt save(Receipt obj) {

        String sql = "INSERT INTO Receipt (total,receive,total_qty,employee_id,member_id,payment_method,change,promotion_id,subtotal,discount)"
                + "VALUES(?, ?, ?, ?, ?, ?, ?,?,?,?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setDouble(1, obj.getTotal());
            stmt.setDouble(2, obj.getReceive());
            stmt.setInt(3, obj.getTotalQty());
            stmt.setInt(4, obj.getEmployeeId());
            if(obj.getMemberId()>0){
                stmt.setInt(5, obj.getMemberId());
            }
            stmt.setString(6, obj.getPaymentMethod());
            stmt.setDouble(7, obj.getChange());
             if(obj.getPromotionId()>0){
                stmt.setInt(8, obj.getPromotionId());
            }
             stmt.setDouble(9, obj.getSubtotal());
             
             stmt.setDouble(10, obj.getDiscount());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInsertedId(stmt);
            obj.setId(id);
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return null;
        }
        return obj;
    }

//    @Override
//    public Receipt update(Receipt obj) {
//        String sql = "UPDATE Receipt"
//                + " SET total = ?, cash = ?, total_qty = ?, employee_id = ?, member_id = ?, payment_method = ?"
//                + " WHERE id = ?";
//        Connection conn = DatabaseHelper.getConnect();
//        try {
//            PreparedStatement stmt = conn.prepareStatement(sql);
//            stmt.setFloat(1, obj.getTotal());
//            stmt.setFloat(2, obj.getReceive());
//            stmt.setInt(3, obj.getTotalQty());
//            stmt.setInt(4, obj.getEmployeeId());
//            stmt.setInt(5, obj.getMemberId());
//            stmt.setString(6, obj.getPaymentMethod());
//            stmt.setInt(7, obj.getId());
////            System.out.println(stmt);
//            int ret = stmt.executeUpdate();
//            System.out.println(ret);
//            return obj;
//        } catch (SQLException ex) {
//            System.out.println(ex.getMessage());
//            return null;
//        }
//    }

    @Override
    public int delete(Receipt obj) {
        String sql = "DELETE FROM Receipt WHERE id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return -1;
    }

    @Override
    public List<Receipt> getAllByName(String name) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Receipt update(Receipt obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

}
