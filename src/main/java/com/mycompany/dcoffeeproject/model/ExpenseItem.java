/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mildm
 */
public class ExpenseItem {

    private int id;
    private String name;
    private Double total;
    private int expenseId;

    public ExpenseItem(int id, String name, Double total, int expenseId) {
        this.id = id;
        this.name = name;
        this.total = total;
        this.expenseId = expenseId;

    }

    public ExpenseItem(String name, Double total, int expenseId) {
        this.id = -1;
        this.name = name;
        this.total = total;
        this.expenseId = expenseId;

    }

    public ExpenseItem(String name, Double total) {
        this.name = name;
        this.total = total;
    }

    public ExpenseItem() {
        this.id = -1;
        this.name = "";
        this.total = 0.0;
        this.expenseId = -1;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public int getExpenseId() {
        return expenseId;
    }

    public void setExpenseId(int expenseId) {
        this.expenseId = expenseId;

    }

    @Override
    public String toString() {
        return "ExpenseItem{" + "id=" + id + ", name=" + name + ", total=" + total + ", expenseId=" + expenseId + '}';
    }

    public static ExpenseItem fromRS(ResultSet rs) {
        ExpenseItem expenseItem = new ExpenseItem();
        try {
            expenseItem.setId(rs.getInt("id"));
            expenseItem.setName(rs.getString("name"));
            expenseItem.setTotal(rs.getDouble("total"));
            expenseItem.setExpenseId(rs.getInt("expense_id"));

        } catch (SQLException ex) {
            Logger.getLogger(ExpenseItem.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return expenseItem;
    }

    public boolean isValid() {
        boolean isNameValid = name != null && name.length() >= 2 && !containsNumbers(name);
        boolean isTotalValid = total != null && total > 0.0;

        return isNameValid && isTotalValid;
    }

    private boolean containsNumbers(String text) {
        return text.chars().anyMatch(Character::isDigit);
    }
}
