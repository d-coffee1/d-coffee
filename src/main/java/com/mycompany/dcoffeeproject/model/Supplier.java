/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class Supplier {

    private int id;
    private String name;

    public Supplier(String name) {
        this.name = name;
    }

    public Supplier() {
        this.id = -1;
        this.name = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static Supplier fromRS(ResultSet rs) {
        Supplier supplier = new Supplier();
        try {
            supplier.setId(rs.getInt("id"));
            supplier.setName(rs.getString("name"));
        } catch (SQLException ex) {
            Logger.getLogger(Supplier.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return supplier;
    }

    @Override
    public String toString() {
        return name;
    }

    public boolean isValid() {
        return this.name.length() >=2 ;
    }

}
