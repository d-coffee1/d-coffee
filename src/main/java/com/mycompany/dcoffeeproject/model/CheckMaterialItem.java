/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thanc
 */
public class CheckMaterialItem {
    private int id;
    private int materialId;
    private Material material;
    private int lastBalance,currentBalance;
    private int checkMaterialId;

    public CheckMaterialItem(int id, int materialId, int lastBalance, int currentBalance, int checkMaterialId) {
        this.id = id;
        this.materialId = materialId;
        this.lastBalance = lastBalance;
        this.currentBalance = currentBalance;
        this.checkMaterialId = checkMaterialId;
    }

    public CheckMaterialItem(){
        this.id = -1;
        this.materialId = 0;
        this.lastBalance = 0;
        this.currentBalance = 0;
        this.checkMaterialId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.materialId = material.getId();
        this.material = material;
    }

    public int getLastBalance() {
        return lastBalance;
    }

    public void setLastBalance(int lastBalance) {
        this.lastBalance = lastBalance;
    }

    public int getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(int currentBalance) {
        this.currentBalance = currentBalance;
    }

    public int getCheckMaterialId() {
        return checkMaterialId;
    }

    public void setCheckMaterialId(int checkMaterialId) {
        this.checkMaterialId = checkMaterialId;
    }

    @Override
    public String toString() {
        return "CheckMaterialItem{" + "id=" + id + ", materialId=" + materialId + ", material=" + material + ", lastBalance=" + lastBalance + ", currentBalance=" + currentBalance + ", checkMaterialId=" + checkMaterialId + '}';
    }
    
    public static CheckMaterialItem fromRS(ResultSet rs) {
        CheckMaterialItem checkMaterialItem = new CheckMaterialItem();
        try {
            checkMaterialItem.setId(rs.getInt("id"));
            checkMaterialItem.setCheckMaterialId(rs.getInt("check_material_id"));
            MaterialDao matDao = new MaterialDao();
            Material material = matDao.getOne(rs.getInt("material_id"));
            checkMaterialItem.setMaterial(material);
            checkMaterialItem.setLastBalance(rs.getInt("last_balance"));
            checkMaterialItem.setCurrentBalance(rs.getInt("current_balance"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterialItem.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkMaterialItem;
    }
    public boolean isValid(){
        return this.currentBalance<=this.lastBalance;
    }
}
