/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thanc
 */
public class Salary {

    private int id;
    private Date date;
    private int employeeId;
    private Employee employee;
    private String employeeName, employeeSurname;
    private Double employeeHourWage;
    private int totalWorkHours;
    private Double totalSalary;
    private List<CheckInOut> checkInOuts;

    public Salary(int id, Date date, int employeeId, String employeeName, String employeeSurname, Double employeeHourWage, int totalWorkHours, Double totalSalary, List<CheckInOut> checkInOuts) {
        this.id = id;
        this.date = date;
        this.employeeId = employeeId;
        this.employeeName = employeeName;
        this.employeeSurname = employeeSurname;
        this.employeeHourWage = employeeHourWage;
        this.totalWorkHours = totalWorkHours;
        this.totalSalary = totalSalary;
        this.checkInOuts = checkInOuts;
    }

    public Salary() {
        this.id = -1;
        this.date = null;
        this.employeeId = 0;
        this.employeeName = "";
        this.employeeSurname = "";
        this.employeeHourWage = 0.00;
        this.totalWorkHours = 0;
        this.totalSalary = 0.00;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeSurname() {
        return employeeSurname;
    }

    public void setEmployeeSurname(String employeeSurname) {
        this.employeeSurname = employeeSurname;
    }

    public Double getEmployeeHourWage() {
        return employeeHourWage;
    }

    public void setEmployeeHourWage(Double employeeHourWage) {
        this.employeeHourWage = employeeHourWage;
    }

    public int getTotalWorkHours() {
        return totalWorkHours;
    }

    public void setTotalWorkHours(int totalWorkHours) {
        this.totalWorkHours = totalWorkHours;
    }

    public Double getTotalSalary() {
        return totalSalary;
    }

    public void setTotalSalary(Double totalSalary) {
        this.totalSalary = totalSalary;
    }

    public List<CheckInOut> getCheckInOuts() {
        return checkInOuts;
    }

    public void setCheckInOuts(List<CheckInOut> checkInOuts) {
        this.checkInOuts = checkInOuts;
    }

    @Override
    public String toString() {
        return "Salary{" + "id=" + id + ", date=" + date + ", employeeId=" + employeeId + ", employeeName=" + employeeName + ", employeeSurname=" + employeeSurname + ", employeeHourWage=" + employeeHourWage + ", totalWorkHours=" + totalWorkHours + ", totalSalary=" + totalSalary + ", checkInOuts=" + checkInOuts + '}';
    }

    public static Salary fromRS(ResultSet rs) {
        Salary salary = new Salary();
        try {
            salary.setId(rs.getInt("id"));
            salary.setDate(rs.getTimestamp("date"));
            salary.setEmployeeName(rs.getString("employee_name"));
            salary.setEmployeeSurname(rs.getString("employee_surname"));
            EmployeeDao employeeDao = new EmployeeDao();
            Employee emp = employeeDao.getOne(rs.getInt("employee_id"));
            salary.setEmployee(emp);
            salary.setEmployeeHourWage(rs.getDouble("employee_hour_wage"));
            salary.setTotalWorkHours(rs.getInt("total_work_hours"));
            salary.setTotalSalary(rs.getDouble("total_salary"));
        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return salary;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.employeeId = employee.getId();
    }

}
