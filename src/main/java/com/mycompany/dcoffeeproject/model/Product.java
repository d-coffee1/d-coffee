/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.CategoryDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class Product {

    private int id;
    private String name;
    private Double price;
    private int categoryId;
    private Category category;
    private String availableType;

    public Product(String name, Double price, int categoryId) {
        this.name = name;
        this.price = price;
        this.categoryId = categoryId;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public Product() {
        this.id = -1;
        this.name = "";
        this.price = 0.0;
        this.categoryId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getAvailableType() {
        return availableType;
    }

    public void setAvailableType(String availableType) {
        this.availableType = availableType;
    }

    public static Product fromRS(ResultSet rs) {
        Product product = new Product();
        try {
            product.setId(rs.getInt("id"));
            product.setName(rs.getString("name"));
            product.setPrice(rs.getDouble("price"));
            product.setCategoryId(rs.getInt("category_id"));
            product.setAvailableType(rs.getString("available_type"));
            CategoryDao categoryDao = new CategoryDao();
            Category category = categoryDao.getOne(product.getCategoryId());
            product.setCategory(category);
        } catch (SQLException ex) {
            Logger.getLogger(Product.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return product;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", categoryId=" + categoryId + ", category=" + category + ", availableType=" + availableType + '}';
    }

    public boolean isValid() {
        boolean isNameValid = name != null && name.length() >= 2 && !containsNumbers(name);
        boolean isPriceValid = price != null && price > 0.0 ;

        return isNameValid && isPriceValid ;
    }

    private boolean containsNumbers(String text) {
        return text.chars().anyMatch(Character::isDigit);
    }

}
