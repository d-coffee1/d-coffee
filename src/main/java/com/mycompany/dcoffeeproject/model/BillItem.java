/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.MaterialDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mildm
 */
public class BillItem {

    private int id;
    private int materialId;
    private String materialName;
    private Double price;
    private int qty;
    private Double total;
    private int billId;
    private Material material;

    public BillItem(int id, int materialId, String materialName, Double price, int qty, Double total, int billId, Material material) {
        this.id = id;
        this.materialId = materialId;
        this.materialName = materialName;
        this.price = price;
        this.qty = qty;
        this.total = total;
        this.billId = billId;
        this.material = material;
    }

    public BillItem(int materialId, String materialName, double price_per_unit, int qty, double total, int billId) {
        this.id = -1;
        this.materialId = materialId;
        this.materialName = materialName;
        this.price = price_per_unit;
        this.qty = qty;
        this.total = total;
        this.billId = billId;

    }

    public BillItem() {
        this.id = -1;
        this.materialId = 0;
        this.materialName = "";
        this.price = 0.0;
        this.qty = 0;
        this.total = 0.0;
        this.billId = 0;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMaterialId() {
        return materialId;
    }

    public void setMaterialId(int materialId) {
        this.materialId = materialId;
    }

    public String getMaterialName() {
        return materialName;
    }

    public void setMaterialName(String materialName) {
        this.materialName = materialName;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        total = qty * price;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.materialId = material.getId();
        this.materialName = material.getName();
        this.material = material;
    }

    @Override
    public String toString() {
        return "BillItem{" + "id=" + id + ", materialId=" + materialId + ", materialName=" + materialName + ", price=" + price + ", qty=" + qty + ", total=" + total + ", billId=" + billId + ", material=" + material + '}';
    }

    public static BillItem fromRS(ResultSet rs) {
        BillItem billItem = new BillItem();
        try {
            billItem.setId(rs.getInt("id"));
            billItem.setPrice(rs.getDouble("price"));
            billItem.setQty(rs.getInt("qty"));
            billItem.setTotal(rs.getDouble("total"));
            billItem.setBillId(rs.getInt("bill_id"));
            //population
            MaterialDao materialDao = new MaterialDao();
            Material material = materialDao.getOne(rs.getInt("material_id"));
            billItem.setMaterial(material);

        } catch (SQLException ex) {
            Logger.getLogger(BillItem.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return billItem;
    }
}
