/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.RoleDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class Employee {

    private int id;
    private String name;
    private String surname;
    private String username;
    private String password;
    private int roleId;
    private Role role;
    private Double hourWage;

    public Employee(String name, String surname, String username, String password, int roleId, Double hourWage) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.roleId = roleId;
        this.hourWage = hourWage;
    }

    public Employee() {
        this.id = -1;
        this.name = "";
        this.surname = "";
        this.username = "";
        this.password = "";
        this.roleId = -1;
        this.hourWage = 0.00;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    public Double getHourWage() {
        return hourWage;
    }

    public void setHourWage(Double hourWage) {
        this.hourWage = hourWage;
    }

    public static Employee fromRS(ResultSet rs) {
        Employee employee = new Employee();
        try {
            employee.setId(rs.getInt("id"));
            employee.setName(rs.getString("name"));
            employee.setSurname(rs.getString("surname"));
            employee.setUsername(rs.getString("username"));
            employee.setPassword(rs.getString("password"));
            employee.setHourWage(rs.getDouble("hour_wage"));
            RoleDao roleDao = new RoleDao();
            Role role = roleDao.getOne(rs.getInt("role_id"));
            employee.setRole(role);
        } catch (SQLException ex) {
            Logger.getLogger(Employee.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return employee;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.roleId = role.getId();
        this.role = role;
    }

    @Override
    public String toString() {
        return "Employee{" + "id=" + id + ", name=" + name + ", surname=" + surname + ", username=" + username + ", password=" + password + ", roleId=" + roleId + ", role=" + role + ", hourWage=" + hourWage + '}';
    }

//    public boolean isValid() {
//        return this.name.length() >= 2
//                && this.surname.length() >= 2
//                && this.username.length() >= 2
//                && this.password.length() >= 4;
//    }
    public boolean isValid() {
        boolean isNameValid = name != null && name.length() >= 2 && !containsNumbers(name);
        boolean isSurnameValid = surname != null && surname.length() >= 2 && !containsNumbers(surname);
        boolean isUsernameValid = username != null && username.length() >= 2 && !containsNumbers(username);
        boolean isPasswordValid = password != null && password.length() >= 4 ;

        return isNameValid && isSurnameValid && isUsernameValid && isPasswordValid;
    }

    private boolean containsNumbers(String text) {
        return text.chars().anyMatch(Character::isDigit);
    }

}
