/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.EmployeeDao;
import com.mycompany.dcoffeeproject.dao.ExpenseItemDao;
import com.mycompany.dcoffeeproject.dao.MemberDao;
import com.mycompany.dcoffeeproject.dao.ReceiptDetailDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class Receipt {

    private int id;
    private Date date;
    private Double subTotal;
    private Double total;
    private Double receive;
    private int totalQty;
    private int employeeId;
    private int memberId;
    private String paymentMethod;
    private Double change;
    private Employee employee;
    private Member member;
    private Double discount;

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }
    private ArrayList<ReceiptDetail> receiptDetails = new ArrayList<ReceiptDetail>();
    private int PromotionId;

//    public Receipt(int id, Date createdDate, Double total, Double receive,int totalQty, int employeeId, int memberId, String paymentMethod, Double change) {
//        this.id = id;
//        this.createdDate = createdDate;
//        this.total = total;
//        this.receive = receive;
//        this.totalQty = totalQty;
//        this.employeeId = employeeId;
//        this.memberId = memberId;
//        this.paymentMethod = paymentMethod;
//        this.change = change;
//    }
//
//    public Receipt(Date createdDate, float total, float receive, int totalQty, int employeeId, int memberId, String paymentMethod, float change) {
//        this.id = -1;
//        this.createdDate = createdDate;
//        this.total = total;
//        this.receive = receive;
//        this.totalQty = totalQty;
//        this.employeeId = employeeId;
//        this.memberId = memberId;
//        this.paymentMethod = paymentMethod;
//        this.change = change;
//    }
//
//    public Receipt(Date createdDate, float total, float receive, int totalQty, int employeeId, int memberId) {
//        this.id = -1;
//        this.createdDate = createdDate;
//        this.total = total;
//        this.receive = receive;
//        this.totalQty = totalQty;
//        this.employeeId = employeeId;
//        this.memberId = memberId;
//    }
//
//    public Receipt(float total, float receive, int totalQty, int employeeId, int memberId) {
//        this.id = -1;
//        this.createdDate = null;
//        this.total = total;
//        this.receive = receive;
//        this.totalQty = totalQty;
//        this.employeeId = employeeId;
//        this.memberId = memberId;
//    }
//
//    public Receipt(float receive, int employeeId, int memberId) {
//        this.id = -1;
//        this.createdDate = null;
//        this.total = 0;
//        this.receive = receive;
//        this.totalQty = 0;
//        this.employeeId = employeeId;
//        this.memberId = memberId;
//    }
//
//    public Receipt(float receive, int employeeId, int memberId, String payment_method) {
//        this.id = -1;
//        this.createdDate = null;
//        this.total = 0;
//        this.receive = receive;
//        this.paymentMethod = "";
//        this.totalQty = 0;
//        this.employeeId = employeeId;
//        this.memberId = memberId;
//    }
    public int getId() {
        return id;
    }

    public Double getSubtotal() {
        return subTotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subTotal = subtotal;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getReceive() {
        return receive;
    }

    public void setReceive(Double receive) {
        this.receive = receive;
    }

    public Double getChange() {
        return change;
    }

    public void setChange(Double change) {
        this.change = change;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getCreatedDate() {
        return date;
    }

    public void setCreatedDate(Date createdDate) {
        this.date = createdDate;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public int getTotalQty() {
        return totalQty;
    }

    public void setTotalQty(int totalQty) {
        this.totalQty = totalQty;
    }

    public int getMemberId() {
        return memberId;
    }

    public void setMemberId(int memberId) {
        this.memberId = memberId;
    }

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
        //ปรับให้ตรงกัน
        this.memberId = member.getId();
    }

    public void clearMember() {
        this.member = null;
        this.memberId = 0;
        this.discount = 0.0;
        calculateSubTotal();
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.employeeId = employee.getId();
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int id) {
        this.employeeId = id;
    }

    public ArrayList<ReceiptDetail> getReceiptDetails() {
        return receiptDetails;
    }

    public void setReceiptDetails(ArrayList receiptDetails) {
        this.receiptDetails = receiptDetails;
    }

    public Receipt() {
        this.id = -1;
        this.date = null;
        this.total = 0.0;
        this.subTotal = 0.0;
        this.receive = 0.0;
        this.totalQty = 0;
        this.paymentMethod = "";
        this.employeeId = 0;
        this.memberId = 0;
        this.PromotionId = 0;
        this.change = 0.0;
        this.discount = 0.0;
    }

    public int getPromotionId() {
        return PromotionId;
    }

    public void setPromotionId(int PromotionId) {
        this.PromotionId = PromotionId;
    }

    @Override
    public String toString() {
        return "Receipt{" + "id=" + id + ", createdDate=" + date + ", subtotal=" + subTotal + ", total=" + total + ", receive=" + receive + ", totalQty=" + totalQty + ", employeeId=" + employeeId + ", memberId=" + memberId + ", paymentMethod=" + paymentMethod + ", change=" + change + ", employee=" + employee + ", member=" + member + ", discount=" + discount + ", receiptDetails=" + receiptDetails + ", PromotionId=" + PromotionId + '}';
    }

    public void addReceiptDetail(ReceiptDetail receiptDetail) {
        receiptDetails.add(receiptDetail);
        calculateSubTotal();
    }

    public void delReceiptDetail() {
        receiptDetails.clear();
        calculateSubTotal();
    }

    public void addReceiptDetail(Product product, String name, String size, String type, String sweet, Double price, int qty) {
//        ReceiptDetail rd = new ReceiptDetail(product.getId(), product.getName(), product.getPrice(), qty, qty * product.getPrice(), -1);
        ReceiptDetail rd = new ReceiptDetail(product.getId(), name, size, type, sweet, price, qty, price * qty);
        receiptDetails.add(rd);
        calculateSubTotal();
    }

    public void removeReceiptDetail(ReceiptDetail receiptDetail) {
        receiptDetails.remove(receiptDetail);
        calculateSubTotal();
    }

    public void calculateSubTotal() {
        int totalQty = 0;
        Double subTotal = 0.0;
        for (ReceiptDetail rd : receiptDetails) {
            subTotal += rd.getTotalPrice();
            totalQty += rd.getQty();
        }
        this.totalQty = totalQty;
        this.subTotal = subTotal;
        this.total = subTotal;
    }

    public static Receipt fromRS(ResultSet rs) {
        Receipt receipt = new Receipt();
        try {
            receipt.setId(rs.getInt("id"));
            receipt.setCreatedDate(rs.getTimestamp("date"));
            receipt.setSubtotal(rs.getDouble("subtotal"));
            receipt.setTotal(rs.getDouble("total"));
            receipt.setReceive(rs.getDouble("receive"));
            receipt.setTotalQty(rs.getInt("total_qty"));
            receipt.setPaymentMethod(rs.getString("payment_method"));
            receipt.setDiscount(rs.getDouble("discount"));
            receipt.setPromotionId(rs.getInt("promotion_id"));
            receipt.setChange(rs.getDouble("change"));
            MemberDao memberDao = new MemberDao();
            EmployeeDao employeeDao = new EmployeeDao();
            Member member = memberDao.getOne(rs.getInt("member_id"));
            Employee employee = employeeDao.getOne(rs.getInt("employee_id"));
            if (member != null) {
                receipt.setMember(member);
            }
            receipt.setEmployee(employee);
            ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
            receipt.setReceiptDetails((ArrayList) receiptDetailDao.getAllByReceiptId(receipt.getId()));
        } catch (SQLException ex) {
            Logger.getLogger(Receipt.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receipt;
    }
}
