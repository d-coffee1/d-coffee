/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.BillItemDao;
import com.mycompany.dcoffeeproject.dao.EmployeeDao;
import com.mycompany.dcoffeeproject.dao.SupplierDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mildm
 */
public class Bill {

    private int id;
    private Date date;
    private String forSaveDate;
    private Double subtotal;
    private Double discount;
    private Double total;
    private int supplierId;
    private int employeeId;
    private Supplier supplier;
    private Employee employee;
    private ArrayList<BillItem> billItems = new ArrayList<BillItem>();

    public Bill() {
        this.id = -1;
        this.date = null;
        this.subtotal = 0.0;
        this.discount = 0.0;
        this.total = 0.0;
        this.supplierId = 0;
        this.employeeId = 0;
    }

    public String getForSaveDate() {
        return forSaveDate;
    }

    public void setForSaveDate(String forSaveDate) {
        this.forSaveDate = forSaveDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getSubtotal() {
        return subtotal;
    }

    public void setSubtotal(Double subtotal) {
        this.subtotal = subtotal;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public int getSupplierId() {
        return supplierId;
    }

    public void setSupplierId(int supplierId) {
        this.supplierId = supplierId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Supplier getSupplier() {
        return supplier;
    }

    public void setSupplier(Supplier supplier) {
        this.supplier = supplier;
        this.supplierId = supplier.getId();

    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.employeeId = employee.getId();

    }

    public ArrayList<BillItem> getBillItems() {
        return billItems;
    }

    public void setBillItems(ArrayList<BillItem> billItems) {
        this.billItems = billItems;
    }

    public void addBillItem(BillItem billItem) {
        billItems.add(billItem);
        calculateSubTotal();
    }

    public void addBillItem(Material material, double pricePerUnit, int qty) {
        BillItem bi = new BillItem(material.getId(), material.getName(), pricePerUnit, qty, qty * pricePerUnit, -1);
        billItems.add(bi);
        calculateSubTotal();
    }

    public void removeBillItem(BillItem billItem) {
        billItems.remove(billItem);
        calculateSubTotal();
    }

    public void delBillItem() {
        billItems.clear();
        calculateSubTotal();
    }

    public void calculateSubTotal() {
        int totalQty = 0;
        Double subtotal = 0.0;
        for (BillItem bi : billItems) {
            subtotal += bi.getTotal();
            totalQty += bi.getQty();
        }
        this.subtotal = subtotal;
        this.total = subtotal;
    }

    @Override
    public String toString() {
        return "Bill{" + "id=" + id + ", date=" + date + ", forSaveDate=" + forSaveDate + ", subtotal=" + subtotal + ", discount=" + discount + ", total=" + total + ", supplierId=" + supplierId + ", employeeId=" + employeeId + ", supplier=" + supplier + ", employee=" + employee + ", billItems=" + billItems + '}';
    }

    public static Bill fromRS(ResultSet rs) {
        Bill bill = new Bill();
        try {
            bill.setId(rs.getInt("id"));
            bill.setDate(rs.getTimestamp("date"));
            bill.setSubtotal(rs.getDouble("subtotal"));
            bill.setDiscount(rs.getDouble("discount"));
            bill.setTotal(rs.getDouble("total"));
            SupplierDao supplierDao = new SupplierDao();
            Supplier supplier = supplierDao.getOne(rs.getInt("supplier_id"));
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.getOne(rs.getInt("employee_id"));
            bill.setSupplier(supplier);
            bill.setEmployee(employee);
            BillItemDao billItemDao = new BillItemDao();
            bill.setBillItems((ArrayList<BillItem>) billItemDao.getByBillId(bill.getId()));

        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return bill;
    }

    public boolean isValid() {
        return this.discount >= 0.0;
    }
}
