/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.BillItemDao;
import com.mycompany.dcoffeeproject.dao.EmployeeDao;
import com.mycompany.dcoffeeproject.dao.ExpenseItemDao;
import com.mycompany.dcoffeeproject.dao.SupplierDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mildm
 */
public class Expense {

    private int id;
    private Date date;
    private Double total;
    private int employeeId;
    private Employee employee;
    private ArrayList<ExpenseItem> expenseItems = new ArrayList<ExpenseItem>();
    
    public Expense() {
        this.id = -1;
        this.date = null;
        this.total = 0.0;
        this.employeeId = -1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getTotal() {
        return total;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
        this.employeeId = employee.getId();
    }

    public ArrayList<ExpenseItem> getExpenseItems() {
        return expenseItems;
    }

    public void setExpenseItems(ArrayList<ExpenseItem> expenseItems) {
        this.expenseItems = expenseItems;
    }

    public void addExpenseItem(String name, Double total) {
        ExpenseItem ep = new ExpenseItem(name, total);
        expenseItems.add(ep);
        
        calTotal();
    }
    
    private void calTotal() {
        Double total = 0.0;
        for (ExpenseItem ep : expenseItems) {
            total += ep.getTotal();
        }
        this.total = total;
    }
    
    @Override
    public String toString() {
        return "Expenses{" + "id=" + id + ", date=" + date + ", total=" + total + ", employeeId=" + employeeId + ", employee=" + employee + '}';
    }

    public static Expense fromRS(ResultSet rs) {
        Expense expense = new Expense();
        try {
            expense.setId(rs.getInt("id"));
            expense.setDate(rs.getTimestamp("date"));
            expense.setTotal(rs.getDouble("total"));
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.getOne(rs.getInt("employee_id"));
            expense.setEmployee(employee);
            ExpenseItemDao expenseItemDao = new ExpenseItemDao();
            expense.setExpenseItems((ArrayList<ExpenseItem>) expenseItemDao.getByExpenseId(expense.getId()));

        } catch (SQLException ex) {
            Logger.getLogger(Bill.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return expense;
    }

}
