/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thanc
 */
public class CheckInOut {

    private int id;
    private Date date;
    private Date checkInTime, checkOutTime;
    private int workHours;
    private int employeeId, salaryId;
    private Employee employee;

    public CheckInOut(Date date, Date checkInTime, Date checkOutTime, int workHours, int employeeId, int salaryId) {
        this.date = date;
        this.checkInTime = checkInTime;
        this.checkOutTime = checkOutTime;

        this.workHours = workHours;
        this.employeeId = employeeId;
        this.salaryId = salaryId;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public CheckInOut() {
        this.checkOutTime = null;
        this.workHours = 0;
        this.employeeId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(Date checkInTime) {
        this.checkInTime = checkInTime;
    }

    public Date getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(Date checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public int getWorkHours() {
        return workHours;
    }

    public void setWorkHours(int workHours) {
        this.workHours = workHours;
    }

    public int getSalaryId() {
        return salaryId;
    }

    public void setSalaryId(int salaryId) {
        this.salaryId = salaryId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "CheckInOut{" + "id=" + id + ", date=" + date + ", checkInTime=" + checkInTime + ", checkOutTime=" + checkOutTime + ", workHours=" + workHours + ", employeeId=" + employeeId + ", salaryId=" + salaryId + ", employee=" + employee + '}';
    }

   

    public static CheckInOut fromRS(ResultSet rs) {
        CheckInOut checkInOut = new CheckInOut();
        try {
            checkInOut.setId(rs.getInt("id"));
            checkInOut.setDate(rs.getTimestamp("date"));
            checkInOut.setCheckInTime(rs.getTimestamp("check_in_time"));
            checkInOut.setCheckOutTime(rs.getTimestamp("check_out_time"));
            checkInOut.setWorkHours(rs.getInt("work_hours"));
            checkInOut.setEmployeeId(rs.getInt("employee_id"));
            checkInOut.setSalaryId(rs.getInt("salary_id"));
            //population
            EmployeeDao employeeDao = new EmployeeDao();
            Employee employee = employeeDao.getOne(checkInOut.getEmployeeId());
            checkInOut.setEmployee(employee);

        } catch (SQLException ex) {
            Logger.getLogger(CheckInOut.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkInOut;
    }

}
