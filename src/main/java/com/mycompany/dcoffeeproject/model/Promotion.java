/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class Promotion {

    private int id;
    private String name;
    private String discription;
    private Date startDate;
    private Date endDate;
    private String startDateStr;
    private String endDateStr;

    public String getStartDateStr() {
        return startDateStr;
    }

    public void setStartDateStr(String startDateStr) {
        this.startDateStr = startDateStr;
    }

    public String getEndDateStr() {
        return endDateStr;
    }

    public void setEndDateStr(String endDateStr) {
        this.endDateStr = endDateStr;
    }
    
    public Promotion(int id, String name, String discription, Date startDate, Date endDate) {
        this.id = id;
        this.name = name;
        this.discription = discription;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Promotion(String name, String discription, Date startDate, Date endDate) {
        this.id = -1;
        this.name = name;
        this.discription = discription;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Promotion() {
        this.id = -1;
        this.name = "";
        this.discription = "";
        this.startDate = null;
        this.endDate = null;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDiscription() {
        return discription;
    }

    public void setDiscription(String discription) {
        this.discription = discription;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
    

    public static Promotion fromRS(ResultSet rs) {
        Promotion promotion = new Promotion();
        try {
            promotion.setId(rs.getInt("id"));
            promotion.setName(rs.getString("name"));
            promotion.setDiscription(rs.getString("description"));
            promotion.setStartDate(rs.getTimestamp("start_date"));
            promotion.setEndDate(rs.getTimestamp("end_date"));
        } catch (SQLException ex) {
            Logger.getLogger(Promotion.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return promotion;
    }

    @Override
    public String toString() {
        return "Promotion{" + "id=" + id + ", name=" + name + ", discription=" + discription + ", startDate=" + startDate + ", endDate=" + endDate + '}';
    }

    public boolean isValid() {
        boolean isNameValid = name != null && name.length() >= 2 && !containsNumbers(name);
        boolean isDiscriptionValid = discription != null && discription.length() >= 2 && !containsNumbers(discription);

        return isNameValid && isDiscriptionValid;
    }

    private boolean containsNumbers(String text) {
        return text.chars().anyMatch(Character::isDigit);
    }

}
