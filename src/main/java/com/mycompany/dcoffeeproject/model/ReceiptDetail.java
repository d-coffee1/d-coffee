/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.ProductDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author werapan
 */
public class ReceiptDetail {

    private int id;
    private Product product;
    private int productId;
    private String name;
    private String size, type, sweet;
    private Double price;
    private int qty;
    private Double totalPrice;
    private int receiptId;
    

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
   

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSweet() {
        return sweet;
    }

    public void setSweet(String sweet) {
        this.sweet = sweet;
    }

    public ReceiptDetail(int productId, String name, String size, String type, String sweet, Double price, int qty, Double totalPrice) {
        this.productId = productId;
        this.name = name;
        this.size = size;
        this.type = type;
        this.sweet = sweet;
        this.price = price;
        this.qty = qty;
        this.totalPrice = totalPrice;
    }

//    public ReceiptDetail(int id, int productId, String productName, Double productPrice, int qty, Double totalPrice, int receiptId) {
//        this.id = id;
//        this.productId = productId;
//        this.productName = productName;
//        this.productPrice = productPrice;
//        this.qty = qty;
//        this.totalPrice = totalPrice;
//        this.receiptId = receiptId;
//    }
//
//    public ReceiptDetail(int productId, String productName, Double productPrice, int qty, Double totalPrice, int receiptId) {
//        this.id = -1;
//        this.productId = productId;
//        this.productName = productName;
//        this.productPrice = productPrice;
//        this.qty = qty;
//        this.totalPrice = totalPrice;
//        this.receiptId = receiptId;
//    }
    public ReceiptDetail() {
        this.id = -1;
        this.productId = 0;
        this.name = "";
        this.size = "";
        this.type = "";
        this.sweet = "";
        this.price = 0.0;
        this.qty = 0;
        this.totalPrice = 0.0;
        this.receiptId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
        totalPrice = qty * price;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getReceiptId() {
        return receiptId;
    }

    public void setReceiptId(int receiptId) {
        this.receiptId = receiptId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.productId = product.getId();
        this.product = product;
    }

    @Override
    public String toString() {
        return "ReceiptDetail{" + "id=" + id + ", product=" + product + ", productId=" + productId + ", name=" + name + ", size=" + size + ", type=" + type + ", sweet=" + sweet + ", price=" + price + ", qty=" + qty + ", totalPrice=" + totalPrice + ", receiptId=" + receiptId + '}';
    }

    public static ReceiptDetail fromRS(ResultSet rs) {
        ReceiptDetail receiptDetail = new ReceiptDetail();
        try {
            receiptDetail.setId(rs.getInt("id"));
            ProductDao productDao = new ProductDao();
            Product product = productDao.getOne(rs.getInt("product_id"));
            receiptDetail.setProduct(product);
            receiptDetail.setName(rs.getString("name"));
            receiptDetail.setSize(rs.getString("size"));
            receiptDetail.setType(rs.getString("type"));
            receiptDetail.setSweet(rs.getString("sweet"));
            receiptDetail.setPrice(rs.getDouble("price"));
            receiptDetail.setQty(rs.getInt("qty"));
            receiptDetail.setTotalPrice(rs.getDouble("total"));
            receiptDetail.setReceiptId(rs.getInt("receipt_id"));

        } catch (SQLException ex) {
            Logger.getLogger(ReceiptDetail.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return receiptDetail;
    }
}
