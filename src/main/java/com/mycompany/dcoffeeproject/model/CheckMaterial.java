/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import com.mycompany.dcoffeeproject.dao.CheckMaterialItemDao;
import com.mycompany.dcoffeeproject.dao.EmployeeDao;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author thanc
 */
public class CheckMaterial {

    private int id;
    private Date date;
    private int employeeId;
    private Employee employee;
    private ArrayList<CheckMaterialItem> checkMaterialItems = new ArrayList();

    public ArrayList<CheckMaterialItem> getCheckMaterialItems() {
        return checkMaterialItems;
    }

    public void setCheckMaterialItems(ArrayList<CheckMaterialItem> checkMaterialItems) {
        this.checkMaterialItems = checkMaterialItems;
    }
    
    public CheckMaterial(int id, Date date, int employeeId) {
        this.id = id;
        this.date = date;
        this.employeeId = employeeId;
    }

    public CheckMaterial() {
        this.id = -1;
        this.date = null;
        this.employeeId = 0;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public int getEmployeeId() {
        return employeeId;
    }

    public void setEmployeeId(int employeeId) {
        this.employeeId = employeeId;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    @Override
    public String toString() {
        return "CheckMaterial{" + "id=" + id + ", date=" + date + ", employeeId=" + employeeId + ", employee=" + employee + ", checkMaterialItems=" + checkMaterialItems + '}';
    }

    public void addCheckMaterialItem(CheckMaterialItem checkMaterialItem){
        checkMaterialItems.add(checkMaterialItem);
    }



    
    
    public static CheckMaterial fromRS(ResultSet rs) {
        CheckMaterial checkMaterial = new CheckMaterial();
        try {
            checkMaterial.setId(rs.getInt("id"));
            checkMaterial.setDate(rs.getTimestamp("date"));
            checkMaterial.setEmployeeId(rs.getInt("employee_id"));  
            EmployeeDao employeeDao = new EmployeeDao();
            Employee emp = employeeDao.getOne(checkMaterial.getEmployeeId());
            checkMaterial.setEmployee(emp);
            CheckMaterialItemDao checkMaterialItemDao = new CheckMaterialItemDao();
            checkMaterial.setCheckMaterialItems((ArrayList<CheckMaterialItem>) checkMaterialItemDao.getByCheckMatId(checkMaterial.getId()));
       
        } catch (SQLException ex) {
            Logger.getLogger(CheckMaterial.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return checkMaterial;
    }

}
