/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class Material {

    private int id;
    private String name;
    private String unit;
    private int balance;
    private int minBalance;
    private String status;

    public Material(String name, String unit, int balance, int minBalance, String status) {
        this.name = name;
        this.unit = unit;
        this.balance = balance;
        this.minBalance = minBalance;
        this.status = status;
    }

    public Material() {
        this.id = -1;
        this.name = "";
        this.unit = "";
        this.balance = 0;
        this.minBalance = 0;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public int getMinBalance() {
        return minBalance;
    }

    public void setMinBalance(int minBalance) {
        this.minBalance = minBalance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public static Material fromRS(ResultSet rs) {
        Material material = new Material();
        try {
            material.setId(rs.getInt("id"));
            material.setName(rs.getString("name"));
            material.setUnit(rs.getString("unit"));
            material.setBalance(rs.getInt("balance"));
            material.setMinBalance(rs.getInt("min_balance"));
            material.setStatus(rs.getString("status"));

        } catch (SQLException ex) {
            Logger.getLogger(Material.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return material;
    }

    @Override
    public String toString() {
        return "Material{" + "id=" + id + ", name=" + name + ", unit=" + unit + ", balance=" + balance + ", minBalance=" + minBalance + ", status=" + status + '}';
    }

    public boolean isValid() {
        boolean isNameValid = name != null && name.length() >= 2 && !containsNumbers(name);
        boolean isBalanceValid = balance>= 0;
        boolean isminBalanceValid = minBalance > 0;

        return isNameValid && isBalanceValid && isminBalanceValid;
    }

    private boolean containsNumbers(String text) {
        return text.chars().anyMatch(Character::isDigit);
    }

}
