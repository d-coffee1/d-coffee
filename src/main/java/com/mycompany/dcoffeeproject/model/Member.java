/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class Member {

    private int id;
    private String name;
    private String surname;
    private String phone;
    private int point;

    public Member(String name, String surname, String phone) {
        this.name = name;
        this.surname = surname;
        this.phone = phone;
    }

    public Member() {
        this.id = -1;
        this.name = "";
        this.surname = "";
        this.phone = "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "Member{" + "id=" + id + ", name=" + name + ", surname=" + surname + ", phone=" + phone + ", point=" + point + '}';
    }

    public static Member fromRS(ResultSet rs) {
        Member member = new Member();
        try {
            member.setId(rs.getInt("id"));
            member.setName(rs.getString("name"));
            member.setSurname(rs.getString("surname"));
            member.setPhone(rs.getString("phone"));
            member.setPoint(rs.getInt("point"));
        } catch (SQLException ex) {
            Logger.getLogger(Member.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        return member;
    }

    public boolean isValid() {
        boolean isNameValid = name != null && name.length() >= 2 && !containsNumbers(name);
        boolean isSurnameValid = surname != null && surname.length() >= 2 && !containsNumbers(surname);
        boolean isPhoneValid = phone != null && phone.length() == 10 && phone.startsWith("0");

        return isNameValid && isSurnameValid && isPhoneValid;
    }

    private boolean containsNumbers(String text) {
        return text.chars().anyMatch(Character::isDigit);
    }
}
