/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.MaterialDao;
import com.mycompany.dcoffeeproject.model.Material;
import java.util.List;

/**
 *
 * @author ส้มส้ม
 */
public class MaterialService {

    MaterialDao materialDao = new MaterialDao();

    public Material getOne(int id) {
        return materialDao.getOne(id);
    }

    public List<Material> getMaterials() {
        return materialDao.getAll();
    }

    public List<Material> getMaterialsASC() {
        return materialDao.getAllMaterialsASC();
    }
    
    public List<Material> getLowbalance() {
        return materialDao.getAllLowBalance();
    }

    public Material addNew(Material editedMaterial) throws ValidateException {
        if (!editedMaterial.isValid()) {
            throw new ValidateException("ชื่อวัตถุดิบต้องมากกว่าเท่ากับ 2 ตัวอักษร และ จำนวนวัตถุดิบ/ขั้นต่ำ ต้องมากกว่า 0  ");
        }
        updateMatStatus(editedMaterial);
        return materialDao.save(editedMaterial);
    }

    public Material update(Material editedMaterial) throws ValidateException {
        if (!editedMaterial.isValid()) {
            throw new ValidateException("ชื่อวัตถุดิบต้องมากกว่าเท่ากับ 2 ตัวอักษร และ จำนวนวัตถุดิบ/ขั้นต่ำ ต้องมากกว่า 0  ");
        }
        updateMatStatus(editedMaterial);
        return materialDao.update(editedMaterial);
    }

    private void updateMatStatus(Material editedMaterial) {
        if (editedMaterial.getBalance() > 0 && editedMaterial.getBalance() < editedMaterial.getMinBalance()) {
            editedMaterial.setStatus("เหลือน้อย");
        } else if (editedMaterial.getBalance() == 0) {
            editedMaterial.setStatus("หมดสต๊อก");
        } else if (editedMaterial.getBalance() >= editedMaterial.getMinBalance()) {
            editedMaterial.setStatus("ปกติ");
        }
    }

    public int delete(Material editedMaterial) {
        return materialDao.delete(editedMaterial);
    }

    public List<Material> searchMaterialsByName(String name) {
        return materialDao.getAllByName(name);
    }
}
