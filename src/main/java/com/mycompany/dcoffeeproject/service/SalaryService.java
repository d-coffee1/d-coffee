/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.SalaryDao;
import com.mycompany.dcoffeeproject.model.CheckInOut;
import com.mycompany.dcoffeeproject.model.Salary;
import com.mycompany.dcoffeeproject.ui.report.ProductReport;
import com.mycompany.dcoffeeproject.ui.report.SalaryReport;
import java.util.List;

/**
 *
 * @author thanc
 */
public class SalaryService {

    SalaryDao salaryDao = new SalaryDao();

    public List<Salary> getSalaries() {
        return salaryDao.getAll();
    }
    public List<Salary> getSalaries(int id) {
        return salaryDao.getAll(id);
    }
    public List<Salary> getSalaries(String startDate, String endDate) {
        String[] startDateParts = startDate.split("-");
        String[] endDateParts = endDate.split("-");

        // Parse the year part and subtract 543
        int startYear = Integer.parseInt(startDateParts[0]) - 543;
        int endYear = Integer.parseInt(endDateParts[0]) - 543;

        // Construct the updated date strings
        String updatedStartDate = startYear + "-" + startDateParts[1] + "-" + startDateParts[2];
        String updatedEndDate = endYear + "-" + endDateParts[1] + "-" + endDateParts[2];
        return salaryDao.getAll(updatedStartDate,updatedStartDate);
    }
    public List<Salary> getSalaries(String startDate, String endDate, int empId) {
        String[] startDateParts = startDate.split("-");
        String[] endDateParts = endDate.split("-");

        // Parse the year part and subtract 543
        int startYear = Integer.parseInt(startDateParts[0]) - 543;
        int endYear = Integer.parseInt(endDateParts[0]) - 543;

        // Construct the updated date strings
        String updatedStartDate = startYear + "-" + startDateParts[1] + "-" + startDateParts[2];
        String updatedEndDate = endYear + "-" + endDateParts[1] + "-" + endDateParts[2];
        return salaryDao.getAll(updatedStartDate,updatedEndDate,empId);
    }
    public Salary addNew(Salary salary) {
        Salary savedSalary = salaryDao.save(salary);
        CheckInOutService checkInOutService = new CheckInOutService();
        for(CheckInOut c:salary.getCheckInOuts()){
            c.setSalaryId(savedSalary.getId());
            checkInOutService.update(c);
        }
        return savedSalary;
    }
    
    public List<SalaryReport> getPaySalary() {
        return salaryDao.getSalary();
    }
    
    public List<SalaryReport> getPaySalary(String begin, String end) {
        String[] startDateParts = begin.split("-");
        String[] endDateParts = end.split("-");
        // Parse the year part and subtract 543
        int startYear = Integer.parseInt(startDateParts[0]) - 543;
        int endYear = Integer.parseInt(endDateParts[0]) - 543;

        // Construct the updated date strings
        String updatedStartDate = startYear + "-" + startDateParts[1] + "-" + startDateParts[2];
        String updatedEndDate = endYear + "-" + endDateParts[1] + "-" + endDateParts[2];
        return salaryDao.getSalary(updatedStartDate, updatedEndDate);
    }
}
