/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.CheckInOutDao;
import com.mycompany.dcoffeeproject.model.CheckInOut;
import java.util.Date;

import java.util.List;

/**
 *
 * @author ส้มส้ม
 */
public class CheckInOutService {

    CheckInOutDao checkInOutDao = new CheckInOutDao();

    public CheckInOut getOne(int id) {
        return checkInOutDao.getOne(id);
    }

    public List<CheckInOut> getCheckInOuts() {
        return checkInOutDao.getAll();
    }

    public List<CheckInOut> getCheckInOutsHistory() {
        return checkInOutDao.getAllHistory();
    }

    public List<CheckInOut> getCheckInOutsHistory(String startDate, String endDate, int empId) {
        String[] startDateParts = startDate.split("-");
        String[] endDateParts = endDate.split("-");

        // Parse the year part and subtract 543
        int startYear = Integer.parseInt(startDateParts[0]) - 543;
        int endYear = Integer.parseInt(endDateParts[0]) - 543;

        // Construct the updated date strings
        String updatedStartDate = startYear + "-" + startDateParts[1] + "-" + startDateParts[2];
        String updatedEndDate = endYear + "-" + endDateParts[1] + "-" + endDateParts[2];
        
        return checkInOutDao.getAllHistory(updatedStartDate, updatedEndDate, empId);
    }

    public List<CheckInOut> getCheckInOutsHistory(String startDate, String endDate) {
        String[] startDateParts = startDate.split("-");
        String[] endDateParts = endDate.split("-");

        // Parse the year part and subtract 543
        int startYear = Integer.parseInt(startDateParts[0]) - 543;
        int endYear = Integer.parseInt(endDateParts[0]) - 543;

        // Construct the updated date strings
        String updatedStartDate = startYear + "-" + startDateParts[1] + "-" + startDateParts[2];
        String updatedEndDate = endYear + "-" + endDateParts[1] + "-" + endDateParts[2];

        // Use the updated strings to fetch the history
        return checkInOutDao.getAllHistory(updatedStartDate, updatedEndDate);

    }

    public List<CheckInOut> getCheckInOutsHistory(int empId) {
        return checkInOutDao.getAllHistory(empId);
    }

    public List<CheckInOut> getCheckInOutsByEmployeeId(int id) {
        return checkInOutDao.getAllByEmpId(id);
    }

    public CheckInOut addNew(CheckInOut editedCheckInOut) {
        return checkInOutDao.save(editedCheckInOut);
    }

    public CheckInOut update(CheckInOut editedCheckInOut) {
        return checkInOutDao.update(editedCheckInOut);
    }

    public int delete(CheckInOut editedCheckInOut) {
        return checkInOutDao.delete(editedCheckInOut);
    }

    public CheckInOut checkOut(CheckInOut editedCheckInOut) {
        Date now = new Date();
        editedCheckInOut.setCheckOutTime(now);
        return checkInOutDao.checkOut(editedCheckInOut);
    }

    public List<CheckInOut> searchCheckInOutsByName(String name) {
        return checkInOutDao.getAllByName(name);
    }

    private void changeYear() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
}
