/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.ProductDao;
import com.mycompany.dcoffeeproject.dao.PromotionDao;
import com.mycompany.dcoffeeproject.model.Product;
import com.mycompany.dcoffeeproject.model.Promotion;
import com.mycompany.dcoffeeproject.ui.report.ProductReport;
import com.mycompany.dcoffeeproject.ui.report.ProductWorseReport;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ส้มส้ม
 */
public class PromotionService {

    PromotionDao promotionDao = new PromotionDao();

    public Promotion getOne(int id) {
        return promotionDao.getOne(id);
    }

    public List<Promotion> getPromotions() {
        return promotionDao.getAll();
    }

    public Promotion addNew(Promotion editedPromotion) throws ValidateException {
        if (!editedPromotion.isValid()) {
            throw new ValidateException("ชื่อโปรโมชั่นและรายละเอียดโปรโมชั่นต้องมากกว่าเท่ากับ 2 ตัวอักษร");
        }
        return promotionDao.save(editedPromotion);
    }

    public Promotion update(Promotion editedPromotion) throws ValidateException {
        if (!editedPromotion.isValid()) {
            throw new ValidateException("ชื่อโปรโมชั่นและรายละเอียดโปรโมชั่นต้องมากกว่าเท่ากับ 2 ตัวอักษร");
        }
        return promotionDao.update(editedPromotion);
    }

    public int delete(Promotion editedPromotion) {
        return promotionDao.delete(editedPromotion);
    }

    public List<Promotion> searchProductsByName(String name) {
        return promotionDao.getAllByName(name);
    }
}
