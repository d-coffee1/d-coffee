/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.CategoryDao;
import com.mycompany.dcoffeeproject.model.Category;
import java.util.List;

/**
 *
 * @author thanc
 */
public class CategoryService {

    CategoryDao categoryDao = new CategoryDao();

    public List<Category> getCategories() {
        return categoryDao.getAll();
    }

    public Category getCategoryByName(String name) {
        return categoryDao.getOneByName(name);
    }

    public Category getOne(int id) {
        return categoryDao.getOne(id);
    }

    public Category addNew(Category category) throws ValidateException {
        if (!category.isValid()) {
            throw new ValidateException("ชื่อหมวดหมู่ต้องมากกว่าเท่ากับ 2 ตัวอักษร ");
        }
        return categoryDao.save(category);
    }

    public List<Category> searchCategoriesByName(String name) {
        return categoryDao.getAllByName(name);
    }

    public int delete(Category editedCategory) {
        return categoryDao.delete(editedCategory);
    }
}
