/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.EmployeeDao;
import com.mycompany.dcoffeeproject.model.Employee;
import java.util.List;

/**
 *
 * @author ส้มส้ม
 */
public class EmployeeService {

    private final EmployeeDao employeeDao = new EmployeeDao();
    public static Employee currentEmployee;

    public Employee getCurrentEmployee() {
        return currentEmployee;
    }

    public void LogOut() {
        currentEmployee = null;
    }

    public Employee checkIn(String username, String password) {
        Employee employee = employeeDao.getOneByUsername(username);
        if (employee != null && employee.getPassword().equals(password)) {
            return employee;
        }
        return null;
    }

    public Employee login(String username, String password) {
        Employee employee = employeeDao.getOneByUsername(username);
        if (employee != null && employee.getPassword().equals(password)) {
            currentEmployee = employee;
            return employee;
        }
        return null;
    }

    public Employee getOne(int id) {
        return employeeDao.getOne(id);
    }

    public List<Employee> getEmployees() {
        return employeeDao.getAll();
    }

    public Employee addNew(Employee editedEmployee) throws ValidateException {
        if (!editedEmployee.isValid()) {
            throw new ValidateException("ชื่อ-สกุล และชื่อผู้ใช้ ต้องมากกว่าหรือเท่ากับ 2 ตัวอักษร และรหัสผ่านต้องมากกว่าหรือเท่ากับ 4 ตัวอักษรขึ้นไป  ");
        }
        return employeeDao.save(editedEmployee);
    }

    public Employee update(Employee editedEmployee) throws ValidateException {
        if (!editedEmployee.isValid()) {
            throw new ValidateException("ชื่อ-สกุล และชื่อผู้ใช้ ต้องมากกว่าหรือเท่ากับ 2 ตัวอักษร และรหัสผ่านต้องมากกว่าหรือเท่ากับ 4 ตัวอักษรขึ้นไป  ");
        }
        return employeeDao.update(editedEmployee);
    }

    public int delete(Employee editedEmployee) {
        return employeeDao.delete(editedEmployee);
    }

    public List<Employee> searchEmployeesByName(String name) {
        return employeeDao.getAllByName(name);
    }
}
