/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.MemberDao;
import com.mycompany.dcoffeeproject.model.Member;
import com.mycompany.dcoffeeproject.ui.report.MemberReport;
import java.util.List;

/**
 *
 * @author ส้มส้ม
 */
public class MemberService {

    MemberDao memberDao = new MemberDao();

    public Member getOne(int id) {
        return memberDao.getOne(id);
    }

    public List<Member> getMembers() {
        return memberDao.getAll();
    }

    public Member addNew(Member editedMember) throws ValidateException {
        if (!editedMember.isValid()) {
            throw new ValidateException("ชื่อหรือนามสกุล ต้องมากกว่า 2 ตัวอักษร และเบอร์โทรต้องเท่ากับ 10 ตัว หรือ เริ่มต้นด้วย 0 ");
        }
        return memberDao.save(editedMember);
    }

    public Member update(Member editedMember) throws ValidateException {
        if (!editedMember.isValid()) {
            throw new ValidateException("ชื่อ-สกุล ต้องมากกว่าหรือเท่ากับ 2 ตัวอักษร และเบอร์โทรต้องเท่ากับ 10 ตัว หรือ เริ่มต้นด้วย 0  ");
        }
        return memberDao.update(editedMember);
    }

    public int delete(Member editedMember) {
        return memberDao.delete(editedMember);
    }

    public List<Member> searchMembersByName(String name) {
        return memberDao.getAllByName(name);
    }

    public Member getOneByPhone(String phone) {
        return memberDao.getOneByPhone(phone);
    }

    public List<MemberReport> getTopSpender() {

        return memberDao.getTopSpender(10);
    }

    public List<MemberReport> getTopSpender(String begin, String end) {
        String[] startDateParts = begin.split("-");
        String[] endDateParts = end.split("-");
        // Parse the year part and subtract 543
        int startYear = Integer.parseInt(startDateParts[0]) - 543;
        int endYear = Integer.parseInt(endDateParts[0]) - 543;

        // Construct the updated date strings
        String updatedStartDate = startYear + "-" + startDateParts[1] + "-" + startDateParts[2];
        String updatedEndDate = endYear + "-" + endDateParts[1] + "-" + endDateParts[2];
        return memberDao.getTopSpender(updatedStartDate, updatedEndDate, 10);
    }
}
