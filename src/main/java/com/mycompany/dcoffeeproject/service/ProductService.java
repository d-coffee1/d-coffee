/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.ProductDao;
import com.mycompany.dcoffeeproject.model.Product;
import com.mycompany.dcoffeeproject.ui.report.ProductReport;
import com.mycompany.dcoffeeproject.ui.report.ProductWorseReport;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ส้มส้ม
 */
public class ProductService {

    ProductDao productDao = new ProductDao();

    public Product getOne(int id) {
        return productDao.getOne(id);
    }

    public List<Product> getProducts() {
        return productDao.getAll();
    }

    public Product addNew(Product editedProduct) throws ValidateException {
        if (!editedProduct.isValid()) {
            throw new ValidateException("ชื่อสินค้าต้องมากกว่าเท่ากับ 2 ตัวอักษร และ ราคาสินค้าต้องมากกว่า 0  ");
        }
        return productDao.save(editedProduct);
    }

    public Product update(Product editedProduct) throws ValidateException {
        if (!editedProduct.isValid()) {
            throw new ValidateException("ชื่อสินค้าต้องมากกว่าเท่ากับ 2 ตัวอักษร และ ราคาสินค้าต้องมากกว่า 0  ");
        }
        return productDao.update(editedProduct);
    }

    public int delete(Product editedProduct) {
        return productDao.delete(editedProduct);
    }

    public List<Product> searchProductsByName(String name) {
        return productDao.getAllByName(name);
    }

    public ArrayList<Product> getProductsOrderByName() {
        return (ArrayList<Product>) productDao.getAll(" name ASC ");
    }

    public ArrayList<Product> getProductsByCategory(int categoryId) {
        return (ArrayList<Product>) productDao.getAllByCategory(categoryId);
    }

    public List<ProductReport> getTopTenProductBestSeller() {
        return productDao.getProductTopten(10);
    }

    public List<ProductReport> getTopTenProductBestSeller(String begin, String end) {
        String[] startDateParts = begin.split("-");
        String[] endDateParts = end.split("-");
        // Parse the year part and subtract 543
        int startYear = Integer.parseInt(startDateParts[0]) - 543;
        int endYear = Integer.parseInt(endDateParts[0]) - 543;

        // Construct the updated date strings
        String updatedStartDate = startYear + "-" + startDateParts[1] + "-" + startDateParts[2];
        String updatedEndDate = endYear + "-" + endDateParts[1] + "-" + endDateParts[2];
        return productDao.getProductTopten(updatedStartDate, updatedEndDate, 10);
    }

    public List<ProductWorseReport> getTopTenProductWorseSeller() {

        return productDao.getProductToptenWorse(10);
    }

    public List<ProductWorseReport> getTopTenProductWorseSeller(String begin, String end) {
        String[] startDateParts = begin.split("-");
        String[] endDateParts = end.split("-");
        // Parse the year part and subtract 543
        int startYear = Integer.parseInt(startDateParts[0]) - 543;
        int endYear = Integer.parseInt(endDateParts[0]) - 543;

        // Construct the updated date strings
        String updatedStartDate = startYear + "-" + startDateParts[1] + "-" + startDateParts[2];
        String updatedEndDate = endYear + "-" + endDateParts[1] + "-" + endDateParts[2];
        return productDao.getProductToptenWorse(updatedStartDate, updatedEndDate, 10);
    }

}
