/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.ExpenseDao;
import com.mycompany.dcoffeeproject.dao.ExpenseItemDao;
import com.mycompany.dcoffeeproject.dao.MemberDao;
import com.mycompany.dcoffeeproject.model.Supplier;
import com.mycompany.dcoffeeproject.model.Expense;
import com.mycompany.dcoffeeproject.model.ExpenseItem;
import com.mycompany.dcoffeeproject.ui.report.MemberReport;
import com.mycompany.dcoffeeproject.ui.report.ProfitReport;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ExpenseService {

    ExpenseDao expenseDao = new ExpenseDao();

    public Expense getById(int id) {
        return expenseDao.getOne(id);
    }

    public List<Expense> getExpenses() {
        return expenseDao.getAll();
    }

    public List<Expense> getExpenses(String startDate, String endDate) {
        String[] startDateParts = startDate.split("-");
        String[] endDateParts = endDate.split("-");

        // Parse the year part and subtract 543
        int startYear = Integer.parseInt(startDateParts[0]) - 543;
        int endYear = Integer.parseInt(endDateParts[0]) - 543;

        // Construct the updated date strings
        String updatedStartDate = startYear + "-" + startDateParts[1] + "-" + startDateParts[2];
        String updatedEndDate = endYear + "-" + endDateParts[1] + "-" + endDateParts[2];
        return expenseDao.getAll(updatedStartDate, updatedEndDate);
    }

    public Expense addNew(Expense editedExpense) throws ValidateException {
        ExpenseItemDao expenseItemDao = new ExpenseItemDao();
        Expense expense = expenseDao.save(editedExpense);
        System.out.println(expense);
        for (ExpenseItem ep : editedExpense.getExpenseItems()) {
            if (!ep.isValid()) {
                throw new ValidateException("ชื่อรายจ่ายต้องมากกว่าเท่ากับ 2 ตัวอักษร และรายจ่ายต้องมากกว่า 0.0 ");
            }
            ep.setExpenseId(expense.getId());
            expenseItemDao.save(ep);
        }
        return expense;
    }


    public List<ProfitReport> getProfit(String begin, String end) {

        String[] startDateParts = begin.split("-");
        String[] endDateParts = end.split("-");
        // Parse the year part and subtract 543
        int startYear = Integer.parseInt(startDateParts[0]) - 543;
        int endYear = Integer.parseInt(endDateParts[0]) - 543;

        // Construct the updated date strings
        String updatedStartDate = startYear + "-" + startDateParts[1] + "-" + startDateParts[2];
        String updatedEndDate = endYear + "-" + endDateParts[1] + "-" + endDateParts[2];
        return expenseDao.getProfitAll(updatedStartDate, updatedEndDate);
    }
    
     public List<ProfitReport> getProfit() {
        return expenseDao.getProfitAll();
    }
}
