/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.MemberDao;
import com.mycompany.dcoffeeproject.dao.ReceiptDao;
import com.mycompany.dcoffeeproject.dao.ReceiptDetailDao;
import com.mycompany.dcoffeeproject.model.Member;
import com.mycompany.dcoffeeproject.model.Receipt;
import com.mycompany.dcoffeeproject.model.ReceiptDetail;
import java.util.List;

/**
 *
 * @author werapan
 */
public class ReceiptService {

    private Member member;

    public Member getMember() {
        return member;
    }

    public void setMember(Member member) {
        this.member = member;
    }
    ReceiptDao receiptDao = new ReceiptDao();

    public Receipt getById(int id) {
     
        return receiptDao.getOne(id);
    }

    public List<Receipt> getReciepts() {
  
        return receiptDao.getAll();
    }

    public List<Receipt> getReciepts(String startDate, String endDate) {

        String[] startDateParts = startDate.split("-");
        String[] endDateParts = endDate.split("-");

        // Parse the year part and subtract 543
        int startYear = Integer.parseInt(startDateParts[0]) - 543;
        int endYear = Integer.parseInt(endDateParts[0]) - 543;

        // Construct the updated date strings
        String updatedStartDate = startYear + "-" + startDateParts[1] + "-" + startDateParts[2];
        String updatedEndDate = endYear + "-" + endDateParts[1] + "-" + endDateParts[2];
        return receiptDao.getAll(updatedStartDate, updatedEndDate);
    }

    public Receipt addNew(Receipt editedReciept) {
       
        ReceiptDetailDao receiptDetailDao = new ReceiptDetailDao();
        Receipt receipt = receiptDao.save(editedReciept);
        for (ReceiptDetail rd : editedReciept.getReceiptDetails()) {
            rd.setReceiptId(receipt.getId());
            receiptDetailDao.save(rd);
        }
        MemberDao memberDao = new MemberDao();
        Member SearchMember = memberDao.getOne(receipt.getMemberId());
        if (SearchMember != null) {
            SearchMember.setPoint((int) (SearchMember.getPoint() + (receipt.getTotal() / 10)));
            if (receipt.getPromotionId() == 1 && receipt.getDiscount() > 0.0) {
                SearchMember.setPoint((int) (SearchMember.getPoint() - receipt.getDiscount()));
            }
            memberDao.update(SearchMember);
        }
        return receipt;
    }

    public Receipt update(Receipt editedReciept) {
      
        return receiptDao.update(editedReciept);
    }

    public int delete(Receipt editedReciept) {
      
        return receiptDao.delete(editedReciept);
    }
}
