/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.CheckMaterialDao;
import com.mycompany.dcoffeeproject.dao.CheckMaterialItemDao;
import com.mycompany.dcoffeeproject.model.CheckMaterial;
import com.mycompany.dcoffeeproject.model.CheckMaterialItem;
import com.mycompany.dcoffeeproject.model.Material;
import java.util.List;

/**
 *
 * @author thanc
 */
public class CheckMaterialService {

    CheckMaterialDao checkMaterialDao = new CheckMaterialDao();
    CheckMaterialItemDao checkMaterialItemDao = new CheckMaterialItemDao();

    public CheckMaterial getOne(int id) {
        return checkMaterialDao.getOne(id);
    }

    public List<CheckMaterial> getCheckMaterials() {
        return checkMaterialDao.getAll();
    }
    
    public List<CheckMaterial> getCheckMaterials(String startDate,String endDate) {
            String[] startDateParts = startDate.split("-");
        String[] endDateParts = endDate.split("-");

        // Parse the year part and subtract 543
        int startYear = Integer.parseInt(startDateParts[0]) - 543;
        int endYear = Integer.parseInt(endDateParts[0]) - 543;

        // Construct the updated date strings
        String updatedStartDate = startYear + "-" + startDateParts[1] + "-" + startDateParts[2];
        String updatedEndDate = endYear + "-" + endDateParts[1] + "-" + endDateParts[2];
        return checkMaterialDao.getAll(updatedStartDate,updatedEndDate);
    }

    public CheckMaterial addNew(CheckMaterial editedCheckMaterial) throws ValidateException {
        MaterialService materialSevice = new MaterialService();
        CheckMaterial checkMaterial = checkMaterialDao.save(editedCheckMaterial);
        for (CheckMaterialItem c : editedCheckMaterial.getCheckMaterialItems()) {
            Material mat = materialSevice.getOne(c.getMaterialId());
            mat.setBalance(c.getCurrentBalance());
            System.out.println(mat);
            if (!c.isValid()) {
                throw new ValidateException("จำนวนปัจจุบันต้องน้อยกว่าหรือเท่ากับจำนวนล่าสุด ");
            }
            if (c.getCurrentBalance() > 0 && c.getCurrentBalance() < mat.getMinBalance()) {
                mat.setStatus("เหลือน้อย");
            } else if (c.getCurrentBalance() == 0) {
                mat.setStatus("หมดสต๊อก");
            } else if (c.getCurrentBalance() >= mat.getMinBalance()) {
                mat.setStatus("ปกติ");
            }
            materialSevice.update(mat);
            c.setCheckMaterialId(checkMaterial.getId());
            checkMaterialItemDao.save(c);
        }
        return checkMaterial;
    }
}
