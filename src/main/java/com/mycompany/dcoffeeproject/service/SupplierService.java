/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.SupplierDao;
import com.mycompany.dcoffeeproject.model.Supplier;
import java.util.List;

/**
 *
 * @author thanc
 */
public class SupplierService {

    SupplierDao supplierDao = new SupplierDao();

    public List<Supplier> getSuppliers() {
        return supplierDao.getAll();
    }

    public Supplier getSupplierByName(String name) {
        return supplierDao.getOneByName(name);
    }

    public Supplier getOne(int id) {
        return supplierDao.getOne(id);
    }

    public Supplier addNew(Supplier supplier) throws ValidateException {
        if (!supplier.isValid()) {
            throw new ValidateException("ชื่อร้านค้าต้องมากกว่า 2 ตัวอักษร ");
        }
        return supplierDao.save(supplier);
    }

    public List<Supplier> searchSuppliersByName(String name) {
        return supplierDao.getAllByName(name);
    }

    public int delete(Supplier editedSupplier) {
        return supplierDao.delete(editedSupplier);
    }

    public Supplier update(Supplier editedSupplier) throws ValidateException {
        if (!editedSupplier.isValid()) {
            throw new ValidateException("ชื่อร้านค้าต้องมากกว่า 2 ตัวอักษร ");
        }
        return supplierDao.update(editedSupplier);
    }
}
