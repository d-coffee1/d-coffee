/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.RoleDao;
import com.mycompany.dcoffeeproject.model.Role;
import java.util.List;

/**
 *
 * @author thanc
 */
public class RoleService {
    RoleDao roleDao = new RoleDao();
    public List<Role> getRoles() {
        return roleDao.getAll();
    }
    
    public Role getRoleByName(String name) {
        return roleDao.getOneByName(name);
    }
    
    public Role getOne(int id) {
        return roleDao.getOne(id);
    }

}
