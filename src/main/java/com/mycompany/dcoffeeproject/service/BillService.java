/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.dcoffeeproject.service;

import com.mycompany.dcoffeeproject.dao.BillDao;
import com.mycompany.dcoffeeproject.dao.BillItemDao;
import com.mycompany.dcoffeeproject.model.Bill;
import com.mycompany.dcoffeeproject.model.BillItem;
import java.util.List;

/**
 *
 * @author werapan
 */
public class BillService {

    BillDao billDao = new BillDao();

    public Bill getById(int id) {
        return billDao.getOne(id);
    }

    public List<Bill> getBills() {
        return billDao.getAll();
    }
    public List<Bill> getBills(String startDate,String endDate) {
        String[] startDateParts = startDate.split("-");
        String[] endDateParts = endDate.split("-");

        // Parse the year part and subtract 543
        int startYear = Integer.parseInt(startDateParts[0]) - 543;
        int endYear = Integer.parseInt(endDateParts[0]) - 543;

        // Construct the updated date strings
        String updatedStartDate = startYear + "-" + startDateParts[1] + "-" + startDateParts[2];
        String updatedEndDate = endYear + "-" + endDateParts[1] + "-" + endDateParts[2];
        return billDao.getAll(updatedStartDate,updatedEndDate);
    }

    public Bill addNew(Bill editedBill) throws ValidateException {
        if (!editedBill.isValid()) {
            throw new ValidateException("ส่วนลดต้องมากกว่าเท่ากับ 0.0 ");
        }
        BillItemDao billItemDao = new BillItemDao();
        Bill bill = billDao.save(editedBill);
        for (BillItem bi : editedBill.getBillItems()) {
            bi.setBillId(bill.getId());
            billItemDao.save(bi);
        }
        return bill;
    }

    public Bill update(Bill editedBill) {
        return billDao.update(editedBill);
    }

    public int delete(Bill editedBill) {
        return billDao.delete(editedBill);
    }
}
